#!/bin/bash

### Command line options
# -d test_dir       use tests from test_dir
# -t timeout        set timeout for single test
# -r                write report to a file
# -f file           run ./release/file
# -b batch          use specific batch
# -s                be silent, output only execution line

exe="./release/track1"
ver="./release/verifier"
testdir="tests1"
instancetimeout=1
timeoutset=0
testdirset=0
testbatchset=0
testbatchtimeout=600
testbatch=
besilent=0

mark_test="\e[1m"
mark_ok="\e[32m"
mark_wrong="\e[31m"
mark_tle="\e[35m"
mark_end="\e[0m"

while getopts "sd:t:rb:f:" opt; do
  case $opt in
    s)
      besilent=1
      ;;
    d) 
      testdir=$OPTARG
      testdirset=1
      ;;
    t)
      instancetimeout=$OPTARG
      timeoutset=1
      ;;
    r)
      timestamp=`date +"%F-%H%M%S"`
      githash=`git rev-parse HEAD`
      filename="results-$timestamp-$githash.txt"
      exec >$filename
      mark_test=
      mark_ok=
      mark_wrong=
      mark_tle=
      mark_end=
      ;;
    b)
      testbatchset=1
      testbatchname=$OPTARG
      if [ "$testbatchname" = "small" ]; then
        testbatch="001 003 005 007 009 011 013 015 017 019"
      elif [ "$testbatchname" = "nederlof" ]; then
        testbatch="011 001 085 087 027 005"
      elif [ "$testbatchname" = "lowopt" ]; then
        testbatch="003 005 011 027 029 031 033 047 051 085 087 115"
      elif [ "$testbatchname" = "nederlof+" ]; then
        testbatch="011 001 085 087 027 005 115 171 173"
      elif [ "$testbatchname" = "perf" ]; then
        testbatch="101 103 105 107 109 111 113 115 117 119 121 123 125 127 129 131"
      elif [ "$testbatchname" = "perf2" ]; then
        testbatch="125 127 129 131 133 135 137 139"
      else
        testbatch="$testbatchname"
      fi
      ;;
    f)
      exe="./release/$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done  

if [ "$testbatchset" -eq "0" ]; then
  tests=`ls $testdir/*.gr`
else
  if [ "$timeoutset" -eq "0" ]; then
     instancetimeout=$testbatchtimeout
  fi
  if [ "$testdirset" -eq "0" ]; then
     testdir="tests1"
  fi
  test_files=()
  for t in $testbatch; do
    test_files+=("$testdir/instance$t.gr")
  done
  tests=${test_files[@]} 
fi

totaltests=0
successtests=0
timetmp=`mktemp`

if [ "$besilent" -eq "0" ]; then
  make -C release

  echo "TIME LIMIT = $instancetimeout"
  if [ "$testbatchset" -eq "1" ]; then
    echo "TEST BATCH = $testbatchname"
  fi
  echo "EXE = $exe"
  echo ""
fi

for i in $tests
do
  echo -e -n "${mark_test}Test "
  printf "%30s" $i
  echo -e -n "${mark_end}:           "
  outfile=${i%.gr}.out
  ulimit -Sv 6000000
  /usr/bin/time -q --output=$timetmp -f "%E" timeout $instancetimeout $exe < $i > $outfile
  ret=$?
  echo -n "(time "
  cat $timetmp | tr "\n" " "
  echo -n ")    "
  if (( $ret == 124 ))
  then
    echo -e "${mark_tle}TIMEOUT${mark_end}"
  elif (( $ret == 0 ))
  then
    cp $outfile $outfile-last
    optfile=${i%.gr}.opt
    if [ ! -f "$optfile" ]
    then
      optfile=""
    fi
    $ver $i $outfile $optfile
    retver=$?
    echo -n "        "
    if (( $retver == 0 ))
    then
      successtests=$(( $successtests + 1 ))
      echo -e "${mark_ok}OK${mark_end}"
    elif (( $retver == 2 ))
    then
      successtests=$(( $successtests + 1 ))
      echo -e "${mark_ok}OK (better than opt?)${mark_end}"
    elif (( $retver == 1 ))
    then
      echo -e "${mark_ok}FEASIBLE (worse than opt)${mark_end}"
    else
      echo -e "${mark_wrong}WRONG${mark_end}"
    fi
  else
    echo -e "${mark_wrong}RUNTIME ERROR${mark_end}"
  fi
  rm -f $outfile
  totaltests=$(( $totaltests + 1 ))
done

if [ "$besilent" -eq "0" ]; then
  echo "-----------------------"
  echo "Passed $successtests out of $totaltests"
fi

