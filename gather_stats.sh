#!/bin/bash

exe=./release/diagnose
testdir="tests1"

if [[ "$#" -ge 1 ]]; then
  exec 1>./$1
fi

make -C release >&2

echo "filename,vertices,edges,terminals,max weight,sum of weights,red_vertices,red_edges,red_terminals,red_max_weight,red_sum_of_weights,approximation"

for i in `ls $testdir/*.gr`
do
  echo "Diagnosing $i" >&2
  $exe -r -f $i -c
done

