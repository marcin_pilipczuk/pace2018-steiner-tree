#ifndef REDUCTIONS_H
#define REDUCTIONS_H

#include "value.h"
#include "graph.h"

class Reducer {
public:
    void reduce(graph_t &g);
    std::vector<weight_t> compute_distances(graph_t &g, node_t &node);
    bool check_too_heavy_edges(graph_t &g, node_t &node);

};

#endif // REDUCTIONS_H