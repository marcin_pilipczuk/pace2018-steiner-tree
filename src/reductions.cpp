#include <set>
#include "reductions.h"

using namespace std;

void Reducer::reduce(graph_t &g) {
    bool changed;
    do {
        changed = false;
        for (auto &node : g) {
            changed |= g.check_multiple_edges(node.get_id());
            changed |= check_too_heavy_edges(g, node);
            if (!g.is_terminal(node.get_id())) {
                if (node.size() == 1) {
                    g.delete_vertex(node.get_id());
                    changed = true;
                } else if (node.size() == 2) {
                    g.suppress_deg2_vertex(node.get_id());
                    changed = true;
                }
            }
        }
    }while(changed);
}

vector<weight_t> Reducer::compute_distances(graph_t &g, node_t &node) {
    vector<weight_t> dist(g.number_of_nodes(), inf);
    set<pair<weight_t, node_idx_t> > s;
    dist[node.get_id().id] = 0;
    s.insert({dist[node.get_id().id], node.get_id()});
    while (!s.empty()) {
        node_idx_t v = s.begin()->second;
        weight_t w = dist[v.id];
        s.erase(s.begin());
        for (auto edge : g[v]) {
            node_idx_t u = edge.dest();
            if (w + edge.weight() < dist[u.id]) {
                s.erase({dist[u.id], u});
                dist[u.id] = w + edge.weight();
                s.insert({dist[u.id], u});
            }
        }
    }
    return dist;
}

bool Reducer::check_too_heavy_edges(graph_t &g, node_t &node) {
    vector<weight_t> dist = compute_distances(g, node);
    vector<pair<node_idx_t, weight_t> > to_delete;
    for (auto edge : node) {
        if (edge.weight() > dist[edge.dest().id]) {
            to_delete.push_back({edge.dest(), edge.weight()});
        }
    }
    for (auto p : to_delete) {
        g.delete_edge(node.get_id(), p.first, p.second);
    }
    return !to_delete.empty();
}
