#include "value.h"
#include "graph.h"

using node_t = graph_t::node_t;

// graph_t definition
std::vector<weight_t> graph_t::weight;
std::vector<id_t> graph_t::neighbour;



// !!!
// Graph modifications are NOT sfae.
// They may affect references!
// Hovever, it may not affect iterators.
// Also it doesn't affect references to nodes
// made from iterator;
//   auto &node = *it;
//   // insted of
//   auto &node = graph[i];
// due to return optimization in iterator::operator*()

node_t graph_t::new_node() {
    //vertex.push_back( node_t(number_of_nodes(), *this) );
    vicinity.emplace_back();
    value.emplace_back();

    return operator[](vicinity.size()-1);
}

// graph_t::node_t definition

node_t::node_t(id_t const pos, graph_t &graph) : id(pos), graph(graph) {
}

graph_t::edge_t graph_t::node_t::add_edge(node_t const &target, weight_t weight) {
    return add_edge(target.get_id(), weight);
}

graph_t::edge_t graph_t::node_t::add_edge(node_idx_t const target, weight_t weight) {
    // TODO: Use graph_t::add_edge
    graph.vicinity[id].push_back( graph_t::weight.size() );
    graph_t::neighbour.push_back( target.id );
    graph_t::weight.push_back(weight);

    graph.vicinity[target.id].push_back( graph_t::weight.size() );
    graph_t::neighbour.push_back(id);
    graph_t::weight.push_back(weight);

    return edge_t( graph_t::weight.size() - 2 );
}


// Reductions
void graph_t::__delete_edge(node_idx_t v, index_t in) {
    index_t last = vicinity[v.id].back();
    neighbour[in] = neighbour[last];
    weight[in] = weight[last];
    vicinity[v.id].pop_back();
}

void graph_t::delete_edge_one_side(node_idx_t v, node_idx_t u, weight_t w) {
    for (unsigned int i = 0; i < vicinity[v.id].size(); ++i) {
        index_t in = vicinity[v.id][i];
        if (neighbour[in] == u.id && weight[in] == w) {
            __delete_edge(v, in);
            break;
        }
    }
}

void graph_t::delete_edge(node_idx_t v, node_idx_t u, weight_t w) {
    delete_edge_one_side(v, u, w);
    delete_edge_one_side(u, v, w);
}

void graph_t::__set_weight(node_idx_t const v, node_idx_t const u, weight_t new_w) {
    for (index_t e : vicinity[v.id]) {
        if (neighbour[e] == u.id) {
            weight[e] = new_w;
            break;
        }
    }
}
void graph_t::set_weight(node_idx_t const v, node_idx_t const u, weight_t new_w) {
    __set_weight(v, u, new_w);
    __set_weight(u, v, new_w);
}

void graph_t::delete_vertex(node_idx_t const v) {
    assert(!is_terminal(v));
    node_t x = (*this)[v];
    for (auto edge : x) {
        delete_edge_one_side(edge.dest(), v, edge.weight());
    }
    vicinity[v.id].clear();
}

bool graph_t::check_multiple_edges(node_idx_t const v) {
    std::vector<std::pair<node_idx_t, weight_t> > seen;
    node_t x = (*this)[v];
    bool ret = false;
    for (auto edge : x) {
        seen.push_back({edge.dest(), edge.weight()});
    }
    std::sort(seen.begin(), seen.end());
    for (unsigned i = 1; i < seen.size(); ++i) {
        if (seen[i].first == seen[i-1].first) {
            ret = true;
            delete_edge(v, seen[i].first, seen[i].second);
        }
    }
    return ret;
}

// Deg2 vertices and output
void graph_t::suppress_deg2_vertex(node_idx_t const v) {
    node_t x = (*this)[v];
    assert(x.size() == 2);
    assert(!is_terminal(v));
    edge_t e0 = x[0], e1 = x[1];
    node_idx_t u0 = e0.dest(), u1 = e1.dest();
    weight_t tot_w = e0.weight() + e1.weight();
    bool substitute = true;
    for (auto edge : (*this)[u0]) {
        if (edge.dest() == u1 && edge.weight() <= tot_w) {
            substitute = false;
        }
    }
    if (substitute) {
        for (auto edge : (*this)[u0]) {
            if (edge.dest() == v) {
                neighbour[edge.id] = u1.id;
                weight[edge.id] = tot_w;
                break;
            }
        }
        for (auto edge : (*this)[u1]) {
            if (edge.dest() == v) {
                neighbour[edge.id] = u0.id;
                weight[edge.id] = tot_w;
                break;
            }
        }
        shortcuts[{u0, u1}] = v;
        shortcuts[{u1, u0}] = v;
    } else {
        delete_edge_one_side(u0, v, e0.weight());
        delete_edge_one_side(u1, v, e1.weight());
    }
    vicinity[v.id].clear();
}

void graph_t::output_solution_edge(node_idx_t const v, node_idx_t const u) {
    if (this->shortcuts.count({v,u}) > 0) {
        node_idx_t w = this->shortcuts[{v,u}];
        output_solution_edge(v, w);
        output_solution_edge(u, w);
    } else {
        std::cout << (v.id + 1) << " " << (u.id + 1) << std::endl;
    }
}

void graph_t::output_solution(std::vector<std::pair<node_idx_t, node_idx_t> > const &sol) {
    for (auto p : sol) {
        output_solution_edge(p.first, p.second);
    }
}

