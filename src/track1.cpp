#include <bits/stdc++.h>

//#define USE_REDUCTIONS
const int NEDERLOF_TERMS_CUTOFF = 27;

#include "value.h"
#include "graph.h"
#include "reductions.h"
#include "solvers.h"


using namespace std;

void output(basic_solution &sol, graph_t &graph) {
    cout << "VALUE " << sol.get_result_weight() << endl;
    graph.output_solution(sol.get_result());
}

enum Solutions {
    SOL_DW,
    SOL_NEDERLOF,
    SOL_FSC,
};

Solutions which_algorithm(graph_t &graph, weight_t apx) {
    // Mem check for DP
    if ((1ULL << graph.number_of_terminals()) * graph.number_of_nodes() * sizeof(int) > 11ULL * (1ULL << 29)) {
        return SOL_NEDERLOF;
    }
    double log_gain = (log2(3.0) - 1.0) * graph.number_of_terminals();
    double log_loss = log2((double)apx) * 2.0 + log2((double)graph.number_of_edges()) - 3.0;
    return log_gain > log_loss ? SOL_NEDERLOF : SOL_DW;
}


int main() {
    graph_t graph;
    graph.read_graph(cin);

#ifdef USE_REDUCTIONS
    Reducer().reduce(graph);
#endif

    weight_t apx = approximate(graph);
    Solutions which = which_algorithm(graph, apx);
    if (which == SOL_NEDERLOF) {
        nederlof ned = nederlof(graph);
        ned.algo(apx);
        output(ned, graph);
    } else if (which == SOL_FSC) {
        dp_fsc dp = dp_fsc(graph);
        dp.algo();
        output(dp, graph);
    } else {
        dp_brute dp = dp_brute(graph);
        dp.algo();
        output(dp, graph);
    }
    return 0;
}
