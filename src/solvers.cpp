#include "solvers.h"

#include <algorithm>
#include <bits/stdc++.h>

using namespace std;

// Generic solver
weight_t basic_solution::get_result_weight() {
    return result_weight;
}
vector<pair<node_idx_t, node_idx_t> > const &basic_solution::get_result() {
    return result;
}

// DP brute
dp_brute::dp_brute(graph_t &_graph) : graph(_graph) {}

void dp_brute::algo() {
    nodes_cnt = graph.number_of_nodes();
    terms_cnt = graph.number_of_terminals() - 1;
    dp.resize(1 << terms_cnt);
    dp[0].resize(nodes_cnt, 0);
    for (int i = 1; i < (1 << terms_cnt); ++i) {
        dp[i].resize(nodes_cnt, inf);
    }
    for (int i = 0; i < terms_cnt; i++)
        dp[1 << i][graph.ith_terminal(i).id] = 0;

    for (int i = 1; i < (1 << terms_cnt); i++)
        calc_one_step(i);

    compute_result();
}

void dp_brute::calc_one_step(int step) { //calculating one step of dp
    int greatest_bit = (1 << most_significant_bit(step));

/*    if (step!=(1 << terms_cnt)-1 && __builtin_popcount(step)*3 > terms_cnt*2)//optimization
        return;*/

    for (int j=step; j>=greatest_bit; j=((j-1)&step))
        for (int i=0; i < nodes_cnt; i++)
            dp[step][i] = min(dp[step][i], dp[j][i] + dp[step^j][i]);

/*    if (__builtin_popcount(step)*2 > terms_cnt)//optimization
        return;*/

    dijkstra(step);
}

void dp_brute::dijkstra(int step) {
    vector<weight_t> &dist = dp[step];
    set<pair<weight_t, node_idx_t> > dij;

    for (auto &node : graph) {
        dij.insert({dist[node.get_id().id], node.get_id()});
    }

    while(!dij.empty()) {
        const node_idx_t v = dij.begin()->second;
        const weight_t cost = dij.begin()->first;
        dij.erase(dij.begin());
        for (auto edge : graph[v]) {
            if (dist[edge.dest().id] > cost + edge.weight()) {
                dij.erase({dist[edge.dest().id], edge.dest()});
                dist[edge.dest().id] = cost + edge.weight();
                dij.insert({dist[edge.dest().id], edge.dest()});
            }
        }
    }
}

void dp_brute::compute_result() {
    node_idx_t root = graph.ith_terminal(terms_cnt);
    result_weight = dp[(1 << terms_cnt) - 1][root.id];
    result.clear();
    compute_res_edges(root.id, (1 << terms_cnt) - 1);
}

int dp_brute::most_significant_bit(int mask) {
    int i = -1;
    while (mask) {
        i++;
        mask >>= 1;
    }
    return i;
}

void dp_brute::compute_res_edges(unsigned int node, int mask) {
    if (!mask)
        return;
    if (graph.is_terminal(node_idx_t{node}) && __builtin_popcount(mask) == 1 && graph.ith_terminal(most_significant_bit(mask)).id == node)
        return;
    for (auto edge : graph[node_idx_t{node}]) {
        if (dp[mask][edge.dest().id] + (int)edge.weight() == dp[mask][node]) {
            result.push_back({node_idx_t{node}, edge.dest()});
            compute_res_edges(edge.dest().id, mask);
            return;
        }
    }
    for (int i=(mask-1)&mask; i; i=(i-1)&mask) {
        if (dp[i][node] + dp[mask^i][node] == dp[mask][node]) {
            compute_res_edges(node, i);
            compute_res_edges(node, mask^i);
            return;
        }
    }
    assert(false);
}

// Nederlof
nederlof::nederlof(graph_t &_graph) : graph(_graph) {}

void nederlof::algo(weight_t cap) {
    nodes_cnt = graph.number_of_nodes();
    terms_cnt = graph.number_of_terminals();
    terminals = graph.terminal_list();
    forbidden.resize(nodes_cnt, false);
    dp.resize(cap + 1);
    dp_deg1.resize(cap + 1);
    for (unsigned int i = 0; i <= cap; ++i) {
        dp[i].resize(nodes_cnt);
        dp_deg1[i].resize(nodes_cnt);
    }

    result_weight = run_dp(cap);
    assert(result_weight <= cap);

    syfilis(0, nodes_cnt);
    mst_on_not_forbidden();
}

void nederlof::syfilis(id_t beg, id_t end) {
    int nonterm_cnt = 0;
    for (id_t i = beg; i < end; ++i) {
        if (!graph.is_terminal(node_idx_t{i})) {
            nonterm_cnt++;
            forbidden[i] = true;
        }
    }
    weight_t check = run_dp(result_weight);
    if (check > result_weight) {
        int cnt = 0;
        id_t middle = beg;
        for (id_t i = beg; i < end; ++i) {
            if (!graph.is_terminal(node_idx_t{i})) {
                cnt++;
                if (cnt == (nonterm_cnt + 1) / 2) {
                    middle = i + 1;
                }
                forbidden[i] = false;
            }
        }
        if (nonterm_cnt > 1) {
            syfilis(beg, middle);
            syfilis(middle, end);
        }
    }
}

void nederlof::mst_on_not_forbidden() {
    vector<bool> connected(nodes_cnt, false);
    vector<weight_t> dist(nodes_cnt, inf);
    vector<node_idx_t> incoming(nodes_cnt);
    dist[terminals[0].id] = 0;
    set<pair<weight_t, node_idx_t> > s;
    s.insert({0, terminals[0]});
    while (!s.empty()) {
        node_idx_t v = s.begin()->second;
        s.erase(s.begin());
        if (v != terminals[0]) {
            result.push_back({v, incoming[v.id]});
        }
        connected[v.id] = true;
        for(auto edge : graph[v]) {
            if (!forbidden[edge.dest().id] && !connected[edge.dest().id] && dist[edge.dest().id] > edge.weight()) {
                s.erase({dist[edge.dest().id], edge.dest()});
                dist[edge.dest().id] = edge.weight();
                incoming[edge.dest().id] = v;
                s.insert({dist[edge.dest().id], edge.dest()});
            }
        }
    }
}

const static unsigned mods[] = {1073741789U, 1073741783U};
static unsigned mod;

static inline unsigned add_mod(unsigned a, unsigned b) {
    unsigned c = a + b;
    if (c >= mod)
        c -= mod;
    return c;
}

static inline unsigned mul_mod(unsigned a, unsigned b) {
    return (unsigned)(((unsigned long long)a * b) % mod);
}

weight_t nederlof::run_dp(weight_t cap) {
    node_idx_t root = terminals.back();
    weight_t sol = cap + 1;
    for (unsigned int mod_i : mods) {
        mod = mod_i;
        vector<unsigned> result(cap + 1, 0U);
        for (unsigned long long mask = 0ULL; mask < (1ULL << (terms_cnt - 1)); ++mask) {
            bool minus = false;
            for (int i = 0; i < terms_cnt - 1; ++i) {
                if ((1ULL << i) & mask) {
                    forbidden[terminals[i].id] = true;
                    minus = !minus;
                }
            }
            for (unsigned int i = 0; i <= cap; ++i) {
                fill(dp[i].begin(), dp[i].end(), 0U);
                fill(dp_deg1[i].begin(), dp_deg1[i].end(), 0U);
            }
            fill(dp[0].begin(), dp[0].end(), 1U);
            for (weight_t w = 1; w <= cap; ++w) {
                for (auto &node : graph) {
                    id_t id = node.get_id().id;
                    if (!forbidden[id]) {
                        for (auto edge : node) {
                            if (!forbidden[edge.dest().id] && edge.weight() <= w) {
                                dp_deg1[w][id] = add_mod(dp_deg1[w][id], dp[w - edge.weight()][edge.dest().id]);
                            }
                        }
                        for (weight_t w2 = 0; w2 < w; ++w2) {
                            dp[w][id] = add_mod(dp[w][id], mul_mod(dp[w2][id], dp_deg1[w - w2][id]));
                        }
                    }
                }
            }
            for (weight_t w = 0; w <= cap; ++w) {
                unsigned r = dp[w][root.id];
                if (minus && r != 0) {
                    r = mod - r;
                }
                result[w] = add_mod(result[w], r);
            }
            for (int i = 0; i < terms_cnt - 1; ++i) {
                forbidden[terminals[i].id] = false;
            }
        }
        for (weight_t w = 0; w <= cap; ++w) {
            if (result[w] != 0) {
                sol = min(sol, w);
                break;
            }
        }
    }
    return sol;
}

weight_t approximate(graph_t &graph) {
    Reducer red;
    unsigned int terms_cnt = graph.number_of_terminals();
    vector<vector<weight_t> > M(terms_cnt, vector<weight_t>(terms_cnt, 0));
    vector<node_idx_t> terms = graph.terminal_list();
    for (unsigned int i = 0; i < terms_cnt; ++i) {
        node_t term = graph[terms[i]];
        vector<weight_t> dist = red.compute_distances(graph, term);
        for (unsigned int j = 0; j < terms_cnt; ++j) {
            M[i][j] = dist[terms[j].id];
        }
    }
    vector<weight_t> curr_dist(terms_cnt, inf);
    vector<bool> connected(terms_cnt, false);
    curr_dist[0] = 0;
    weight_t result = 0;
    for (unsigned int i = 0; i < terms_cnt; ++i) {
        weight_t best = inf;
        int best_ind = -1;
        for (unsigned int j = 0 ; j < terms_cnt; ++j) {
            if (!connected[j] && curr_dist[j] < best) {
                best = curr_dist[j];
                best_ind = j;
            }
        }
        assert(best_ind >= 0);
        result += curr_dist[best_ind];
        connected[best_ind] = true;
        for (unsigned int j = 0; j < terms_cnt; ++j) {
            if (!connected[j] && curr_dist[j] > M[best_ind][j]) {
                curr_dist[j] = M[best_ind][j];
            }
        }
    }
    return result;
}

// DP FSC
dp_fsc::dp_fsc(graph_t &_graph) : dp_brute(_graph) {}

void dp_fsc::algo() {
    nodes_cnt = graph.number_of_nodes();
    terms_cnt = graph.number_of_terminals() - 1;
    vector<vector<int64_t> > fsc1, fsc2;
    dp.resize(1 << terms_cnt);
    fsc1.resize(1 << terms_cnt);
    fsc2.resize(1 << terms_cnt);
    for (int i = 0; i < (1 << terms_cnt); ++i) {
        dp[i].resize(nodes_cnt, i == 0 ? 0 : inf);
        fsc1[i].resize(nodes_cnt);
        fsc2[i].resize(nodes_cnt);
    }
    for (int i = 0; i < terms_cnt; i++) {
        dp[1 << i][graph.ith_terminal(i).id] = 0;
        dijkstra(1 << i);
    }

    node_idx_t root = graph.ith_terminal(terms_cnt);
    unsigned int bound = 1;
    while(dp[(1 << terms_cnt) - 1][root.id] > bound) {
        for (unsigned int b1 = 0; b1 < bound; ++b1) {
            unsigned int b2 = bound - b1;
            for (int i = 0; i < (1 << terms_cnt); ++i) {
                for (int v = 0; v < nodes_cnt; ++v) {
                    fsc1[i][v] = (dp[i][v] <= b1) ? 1 : 0;
                    fsc2[i][v] = (dp[i][v] <= b2) ? 1 : 0;
                }
            }
            for (int a = 0; a < terms_cnt; ++a) {
                for (int i = 0; i < (1 << (terms_cnt - 1)); ++i) {
                    int j = (i & ((1 << a) - 1)) | ((i >> a) << (a + 1)); // add 0 at a-th bit
                    for (int v = 0; v < nodes_cnt; ++v) {
                        fsc1[j | (1 << a)][v] += fsc1[j][v];
                        fsc2[j | (1 << a)][v] += fsc2[j][v];
                    }
                }
            }
            for (int i = 0; i < (1 << terms_cnt); ++i) {
                for (int v = 0; v < nodes_cnt; ++v) {
                    fsc1[i][v] *= fsc2[i][v];
                }
            }
            for (int a = 0; a < terms_cnt; ++a) {
                for (int i = 0; i < (1 << (terms_cnt - 1)); ++i) {
                    int j = (i & ((1 << a) - 1)) | ((i >> a) << (a + 1)); // add 0 at a-th bit
                    for (int v = 0; v < nodes_cnt; ++v) {
                        fsc1[j | (1 << a)][v] -= fsc1[j][v];
                    }
                }
            }
            for (int i = 0; i < (1 << terms_cnt); ++i) {
                for (int v = 0; v < nodes_cnt; ++v) {
                    if (fsc1[i][v] > 0 && dp[i][v] > bound) {
                        dp[i][v] = bound;
                    }
                }
            }
        }
        for (int step = 1; step < (1 << terms_cnt); ++step) {
            for (auto &v : graph) {
                if (dp[step][v.get_id().id] == bound) {
                    for (auto edge : v) {
                        if (dp[step][v.get_id().id] + edge.weight() < dp[step][edge.dest().id]) {
                            dp[step][edge.dest().id] = dp[step][v.get_id().id] + edge.weight();
                        }
                    }
                }
            }
            //dijkstra(step);
        }
        bound++;
    }
    compute_result();
}

