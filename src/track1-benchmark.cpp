#include <bits/stdc++.h>
using namespace std;
using ll=int;//TODO consider

long long inf;//three times bigger than sum of edges' weights

int nodes_cnt, edges_cnt, terms_cnt;

vector < vector < pair <int,ll> > > graph;//indexed starting from 1
vector <int> terminals;//list of terminals

void read_input()
{
	string s;
	while(1)
	{
		cin >> s;
		if (s=="Nodes")
		{
			cin >> nodes_cnt;
			graph.resize(nodes_cnt+1);
		}
		if (s=="Edges")
		{
			cin >> edges_cnt;
			for (int i=1; i<=edges_cnt; i++)
			{
				int a, b;
				long long c;
				cin >> s >> a >> b >> c;
				graph[a].push_back({b, c});
				graph[b].push_back({a, c});
				inf+=c;
			}
			inf*=3;
		}
		if (s=="Terminals")
		{
			cin >> s >> terms_cnt;
			for (int i=1; i<=terms_cnt; i++)
			{
				int a;
				cin >> s >> a;
				terminals.push_back(a);
			}
			break;
		}
	}
}

struct dp_brute//O(3^t*n+2^t*m*log(m))
{
	vector < vector <ll> > dp;
	vector <int> is_terminal;
	void algo()
	{
		dp.resize(nodes_cnt+1);
		for (int i=1; i<=nodes_cnt; i++)
		{
			dp[i].resize(1<<terms_cnt, inf);
			dp[i][0]=0;
		}
		for (int i=0; i<terms_cnt; i++)
			dp[terminals[i]][1<<i]=0;
			
		for (int i=1; i<(1<<terms_cnt); i++)
			calc_one_step(i);
		
		write_result();
	}
	void calc_one_step(int step)//calculating one step of dp
	{
		int greatest_bit=1;
		while(greatest_bit*2<=step)
			greatest_bit*=2;
		
		if (step!=(1<<terms_cnt)-1 && __builtin_popcount(step)*3>terms_cnt*2)//optimization
			return;
			
		for (int i=1; i<=nodes_cnt; i++)
			for (int j=step; j>=greatest_bit; j=((j-1)&step))
				dp[i][step]=min(dp[i][step], dp[i][j]+dp[i][step^j]);
				
		if (__builtin_popcount(step)*2>terms_cnt)//optimization
			return;
		
		dijkstra(step);
	}
	void dijkstra(int step)
	{
		priority_queue < pair <ll,int> > dij;
		
		for (int i=1; i<=nodes_cnt; i++)
			for (const auto &j : graph[i])
				if (dp[i][step]+j.second<dp[j.first][step])
					dij.push({-dp[i][step]-j.second, j.first});
					
		while(!dij.empty())
		{
			const int v=dij.top().second;
			const ll cost=-dij.top().first;
			dij.pop();
			if (dp[v][step]<=cost)
				continue;
			dp[v][step]=cost;
			for (const auto &i : graph[v])
				dij.push({-cost-i.second, i.first});
		}
	}
	void write_result()
	{
		ll res=inf;
		int where_res=-1;
		for (int i=1; i<=nodes_cnt; i++)
		{
			if (dp[i][(1<<terms_cnt)-1]<res)
			{
				res=dp[i][(1<<terms_cnt)-1];
				where_res=i;
			}
		}
		cout << "VALUE " << res << endl;
		
		is_terminal.resize(nodes_cnt+1, -1);
		for (int i=0; i<terms_cnt; i++)
			is_terminal[terminals[i]]=i;
		write_res_edges(where_res, (1<<terms_cnt)-1);
	}
	void write_res_edges(int node, int mask)
	{
		if (!mask)
			return;
		if (is_terminal[node]>=0 && mask==(1<<is_terminal[node]))
			return;
		for (const auto &i : graph[node])
		{
			if (dp[i.first][mask]+i.second==dp[node][mask])
			{
				cout << node << " " << i.first << endl;
				write_res_edges(i.first, mask);
				return;
			}
		}
		for (int i=(mask-1)&mask; i; i=(i-1)&mask)
		{
			if (dp[node][i]+dp[node][mask^i]==dp[node][mask])
			{
				write_res_edges(node, i);
				write_res_edges(node, mask^i);
				return;
			}
		}
		assert(false);
	}
};

int main()
{
	read_input();
	if (terms_cnt>27)
	{
		cout << ":(" << endl;
		return 0;
	}
	dp_brute().algo();
	return 0;
}
