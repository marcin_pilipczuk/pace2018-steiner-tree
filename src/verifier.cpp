#include <bits/stdc++.h>
#include <iomanip>
using namespace std;

const int value_width = 7;
const int ratio_precision = 6;

int nodes_cnt, edges_cnt, terms_cnt;

map<pair<int, int>, long long> graph;
vector <int> terminals;

void read_input(char *filename) {
    fstream fin;
    fin.open(filename, fstream::in);
	string s;
	while(fin >> s) {
		if (s == "Nodes") {
			fin >> nodes_cnt;
		}
		if (s == "Edges") {
			fin >> edges_cnt;
			for (int i = 1; i <= edges_cnt; i++){
				int a, b;
				long long c;
				fin >> s >> a >> b >> c;
                if (graph.count({a, b}))
                    c = min(graph[{a,b}], c);
                graph[{a, b}] = c;
                graph[{b, a}] = c;
			}
		}
		if (s == "Terminals") {
			fin >> s >> terms_cnt;
			for (int i = 1; i <= terms_cnt; i++) {
				int a;
				fin >> s >> a;
				terminals.push_back(a);
			}
		}
        if (s == "EOF")
            break;
	}
}

int fau_find(vector<int> &fau, int x) {
    return fau[x] < 0 ? x : fau[x] = fau_find(fau, fau[x]);
}

void fau_join(vector<int> &fau, int x, int y) {
    x = fau_find(fau, x);
    y = fau_find(fau, y);
    if (x != y) {
        if (fau[x] > fau[y])
            swap(x, y);
        if (fau[x] == fau[y])
            fau[x]--;
        fau[y] = x;
    }
}

long long read_solution(char *filename) {
    fstream fin;
    fin.open(filename, fstream::in);
    string s;
    fin >> s;
    if (s != "VALUE") {
        cout << "  No VALUE line, read instead: " << s;
        return -1;
    }
    long long claimed_val, real_val = 0;
    fin >> claimed_val;
    vector<int> fau(nodes_cnt +1, -1);
    int a, b;
    while (fin >> a >> b) {
        if (!graph.count({a, b})) {
            cout << "  No such edge in the graph: " << a << " " << b;
            return -1;
        }
        fau_join(fau, a, b);
        real_val += graph[{a, b}];
    }
    if (real_val != claimed_val) {
        cout << "  Wrong claimed value: claimed " << claimed_val << ", real " << real_val;
        return -1;
    }
    int term_leader = -1;
    for (auto t : terminals) {
        int s = fau_find(fau, t);
        if (term_leader < 0)
            term_leader = s;
        else if (term_leader != s) {
            cout << "  Terminals not connected, terminal " << t << " separated from first terminal";
            return -1;
        }
    }
    return real_val;
}

long long read_opt(char *filename) {
    fstream fin;
    fin.open(filename, fstream::in);
    string s;
    fin >> s; assert(s == "VALUE");
    long long opt_val;
    fin >> opt_val;
    return opt_val;
}


int main(int argc, char *argv[]) {
	read_input(argv[1]);
    long long sol_val;
    if ((sol_val = read_solution(argv[2])) < 0)
        return -1;
    cout << " sol_value=" << setw(value_width) << sol_val << ", ";
    if (argc > 3) {
        long long opt_val = read_opt(argv[3]);
        cout << " opt_value=" << setw(value_width) << opt_val << ", ";
        double ratio = (double)sol_val / (double)opt_val;
        cout << "ratio=" << fixed << setprecision(ratio_precision) << setw(ratio_precision + 2) << ratio << ", ";
        if (opt_val == sol_val) {
            cout << "OPT";
            return 0;
        }
        if (opt_val > sol_val) {
            cout << "better than OPT?";
            return 2;
        }
        else {
            cout << "worse than OPT";
            return 1;
        }
    }
    else {
        cout << "ratio=" << fixed << setprecision(ratio_precision) << setw(ratio_precision + 2) << (double)1.0 << ", optimum unknown";
        return 0;
    }
}
