#include <bits/stdc++.h>
#include <boost/program_options.hpp>

#include "value.h"
#include "graph.h"
#include "reductions.h"
#include "solvers.h"
using namespace std;

namespace po = boost::program_options;

const int field_width=6;
const int value_field_width=8;

bool print_csv = false;
bool reduce = false;
string input_file = "stdin";

void parse_commandline_options(int argc, const char * const *argv){
    po::options_description desc("Supported options");
    desc.add_options()
            ("input-file,f", po::value<string>(), "file to read")
            ("help,h", "produce help message")
            ("csv,c", "print output as CSV line")
            ("reduce,r", "check also after reductions")
            ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")){
        cout << desc << endl;
        exit(0);
    }
    if (vm.count("csv"))
        print_csv = true;
    if (vm.count("reduce"))
        reduce = true;
    if (vm.count("input-file")) {
        if (!freopen(vm["input-file"].as<string>().c_str(), "r", stdin)) {
            cout << "Error opening file: " << vm["input-file"].as<string>() << endl;
            exit(0);
        }
        input_file = vm["input-file"].as<string>();
    }
}

int nodes_cnt, edges_cnt, terms_cnt;
long long max_weight, sum_weights;

void analyse(graph_t &graph) {
    nodes_cnt = 0;
    edges_cnt = 0;
    sum_weights = 0;
    max_weight = 0;
    terms_cnt = graph.number_of_terminals();
    for (auto &node : graph) {
        if (node.size() > 0) {
            nodes_cnt++;
        }
        edges_cnt += node.size();
        for (auto edge : node) {
            max_weight = max(max_weight, (long long)edge.weight());
            sum_weights += edge.weight();
        }
    }
    sum_weights /= 2;
    edges_cnt /= 2;
}

void output_data(void) {
    if (print_csv) {
        cout << "," << nodes_cnt << "," << edges_cnt << "," << terms_cnt;
        cout << "," << max_weight << "," << sum_weights;
    } else {
        cout << "    nodes=" << setw(field_width) << nodes_cnt;
        cout << "    edges=" << setw(field_width) << edges_cnt;
        cout << "    terminals=" << setw(field_width) << terms_cnt;
        cout << "    max_weight=" << setw(value_field_width) << max_weight;
        cout << "    sum_weights=" << setw(value_field_width) << sum_weights;
    }

}

int main(int argc, const char * const *argv){
    parse_commandline_options(argc, argv);
    graph_t graph;
    graph.read_graph(cin);
    analyse(graph);

    if (!print_csv) {
        cout << "File ";
    }
    cout << input_file;

    output_data();

    if (reduce) {
        Reducer().reduce(graph);
        analyse(graph);
        if (!print_csv) {
            cout << " ==AFTER REDUCTIONS== ";
        }
        output_data();
    }

    weight_t apx = approximate(graph);

    if (print_csv) {
        cout << "," << apx;
    } else {
        cout << "    approximation=" << setw(value_field_width) << apx;
    }
    cout << endl;
    return 0;
}
