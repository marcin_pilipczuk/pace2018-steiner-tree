#include "value.h"
#include "graph.h"

int main() {
    graph_t graph;

    //graph[ 7 ]; -- doesn't work! :>
    graph[ node_idx_t{7} ]; // As fast as above... should be.
    // node_idx_t has operator< and operator== and operator!=

    for(auto &node : graph) {
        node_idx_t actual_node_id = node.get_id();

        for(auto edge : graph[actual_node_id]) {
            node_idx_t target = edge.dest();
            weight_t weight = edge.weight();
            // OR
            std::tie(weight, target) = edge.get();
        }

        // OR
        for(auto edge : node) {
            // ...
        }
    }

    for(auto &node : graph) { // graph.begin() works to...
        for(auto it = node.begin(); it != node.end(); ++it) {
            node_idx_t target = it->dest();
            weight_t weight = it->weight();
            // OR
            std::tie(weight, target) = it->get();
        }
    }
}
