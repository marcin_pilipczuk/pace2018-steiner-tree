#ifndef SOLVERS_H
#define SOLVERS_H

#include "value.h"
#include "graph.h"
#include "reductions.h"

class basic_solution {
protected:
    std::vector<std::pair<node_idx_t, node_idx_t> > result;
    weight_t result_weight;

public:
    weight_t get_result_weight();
    std::vector<std::pair<node_idx_t, node_idx_t> > const &get_result();
};

class dp_brute : public basic_solution { //O(3^t*n+2^t*m*log(m))
public:
    dp_brute(graph_t &_graph);
    void algo();

protected:
    std::vector < std::vector <weight_t> > dp;
    std::vector <int> is_terminal;
    graph_t &graph;
    int terms_cnt, nodes_cnt;

    void calc_one_step(int step);
    void dijkstra(int step);
    void compute_result();
    int most_significant_bit(int mask);
    void compute_res_edges(unsigned int node, int mask);
};

class nederlof : public basic_solution { // 2^t * opt^2 * m^2
public:
    nederlof(graph_t &_graph);
    void algo(weight_t cap);

private:
    graph_t &graph;
    std::vector<bool> forbidden;
    std::vector<std::vector<unsigned> > dp, dp_deg1;
    std::vector<node_idx_t> terminals;
    int terms_cnt, nodes_cnt;

    weight_t run_dp(weight_t cap);
    void mst_on_not_forbidden();
    void syfilis(id_t beg, id_t end);
};

class dp_fsc : public dp_brute {
public:
    dp_fsc(graph_t &_graph);
    void algo();

};

// 2 - approximation
weight_t approximate(graph_t &graph);

#endif // SOLVERS_H