#ifndef G_GRAPH_H
#define G_GRAPH_H

#ifndef VALUE_H
#error "value_t should be defined"
#endif

#include <algorithm>
#include <iterator>
#include <vector>
#include <tuple>
#include <map>
#include <cassert>
#include <iostream>

/* BIG TODO:
 * - Add fast node_iterator and edge_iterator
 * - Test implementation (as it's not tested)
 * - No Terminals implemented (!), add reading Terminals
 */

/* INTERFACE:
 * graph_t
 *   node_t
 *   edge_t
 *
 */

using id_t = uint32_t;
using weight_t = uint32_t;
using index_t = uint32_t;  //TODO: size_t may be optimal choice
using invalid_t = decltype(nullptr);

const int inf = 1001*1000*1000;

class node_idx_t {
public:
    id_t id; // Don't use it as long as you don't have to
    // Why it's not private? Well... bucketsort or something...
    // We need fast code and it may be helpful. Don't abuse it!

    inline bool operator<(node_idx_t const &b) const {
        return id < b.id;
    }

    inline bool operator==(node_idx_t const &b) const {
        return id == b.id;
    }

    inline bool operator!=(node_idx_t const &b) const {
        return id != b.id;
    }
};

class graph_t {
public:
    class node_t;
    class edge_t;
    class iterator;

    graph_t(graph_t const &) = delete;
    graph_t(graph_t &&) = delete;
    graph_t() = default;

    template<typename InputStreamT>
    void read_graph(InputStreamT &in);
    // Read input stream form in and create graph with nodes numbered from zero.

    inline node_t operator[](node_idx_t const id);
    //inline node_t const operator[](node_idx_t const id) const; -- needs modifications
    // Returns representation of ith vertex.

    iterator begin();
    iterator end();
    // Returns iterator to begin / end

    node_t new_node();

    id_t number_of_nodes();
    index_t number_of_edges();

    // TERMINALS

    inline void add_terminal(node_idx_t const id);
    inline bool is_terminal(node_idx_t const id);
    inline id_t number_of_terminals();
    inline node_idx_t ith_terminal(id_t const id);
    inline std::vector<node_idx_t> const &terminal_list();

    // DIRECT (fast) functions
    // This functions may (not must) be faster
    // then "standard" way.
    // Try to avoid it as long as it's possible.
    // Optymizations on the end, not start.

    inline void add_edge(node_idx_t const v, node_idx_t const w, weight_t const weight);
    inline edge_t edge_of(node_idx_t node_id, index_t edge_id);
    // Returns ith edge from nth node
    // Standard way: graph[node_id][edge_id]

    std::vector<edge_t> &edge_of(node_idx_t node_id);
    // Returns all edges from nth node.
    // Do NOT use it if not necessary or you don't
    // know what you are doing!
    // Doesnt have standard way, do for(edge : node) instead.

    // Reductions
    void set_weight(node_idx_t const v, node_idx_t const u, weight_t new_w);
    void delete_vertex(node_idx_t const v);
    void delete_edge(node_idx_t v, node_idx_t u, weight_t w);
    bool check_multiple_edges(node_idx_t const v);
    // Shortcutting degree 2 vertices and remembering it for solution output.
    void suppress_deg2_vertex(node_idx_t const v);
    void output_solution_edge(node_idx_t const v, node_idx_t const u);
    void output_solution(std::vector<std::pair<node_idx_t, node_idx_t> > const &sol);

private:
    static std::vector<weight_t> weight;
    static std::vector<id_t> neighbour;

    std::map<std::pair<node_idx_t, node_idx_t>, node_idx_t> shortcuts;

    graph_t &operator=(graph_t const &) = delete;
    std::vector<std::vector<index_t>> vicinity;
    std::vector<value_t> value;
    // std::vector<node_t> vertex; TODO wasn't that faster?

    std::vector<node_idx_t> terminal; 

    inline node_t operator[](id_t const id);
    //inline node_t const operator[](id_t const id) const; -- needs modifications

    // Reductions
    void __set_weight(node_idx_t const v, node_idx_t const u, weight_t new_w);
    void __delete_edge(node_idx_t v, index_t i);
    void delete_edge_one_side(node_idx_t v, node_idx_t u, weight_t w);
};

class graph_t::edge_t {
    friend graph_t::node_t;
    friend graph_t;
    index_t id;

    inline edge_t(index_t const id);
public:

    inline weight_t weight() const;
    inline node_idx_t dest() const;

    inline std::tuple<weight_t, node_idx_t> get() const;
    // Returns ith edge with end node and weight
    // USEAGE:
    // std::get<node_t>(...) - to get node
    // std::get<weight_t>(...) - to get weight
    // OR, when node & weight defined
    // std::tie(node, weight) = ...[i] - to get both

    inline bool operator==(edge_t const &) const;
    inline bool operator!=(edge_t const &) const;
};

class graph_t::node_t {
    friend graph_t;

    id_t id;
    graph_t &graph;
    node_t(id_t const pos, graph_t &graph);

public:
    class iterator;

    node_t(node_t &&) = default;
    node_t(node_t const &) = default;
    node_t() = delete; // Use imaginary_node insted
    // TODO: create imaginary node

    inline index_t size() const;
    // Returns number of neighbours

    inline weight_t weight_of(id_t const pos) const;
    // Returns weight of ith edge from that node.

    inline value_t &value();
    // Returns object value_t connected with node.

    inline node_t dest_of(id_t const pos);
    // Returns neighbour node from ith edge.

    inline node_idx_t get_id() const;
    // Returns node id, as fast as int

    edge_t add_edge(node_t const &target, weight_t weight);
    edge_t add_edge(node_idx_t const target, weight_t weigtht);
    // Creates edge

    inline edge_t operator[](id_t const pos) const;
    // Returns ith edge

    inline bool operator==(node_t const &node) const;
    inline bool operator!=(node_t const &node) const;
    // Checks if a.id == b.id, if a, b in different graphs -- UB

    inline iterator begin();
    inline iterator end();
};

class graph_t::iterator : protected graph_t::node_t, public std::iterator<
                        std::random_access_iterator_tag,    // iterator_category
                        node_t,                           // value_type
                        int32_t,                            // difference_type
                        node_t *,                            // pointer
                        node_t &                             // reference
> {
    friend graph_t;
    using graph_t::node_t::id;

public:

    node_t &operator[](id_t const) = delete;

    inline explicit iterator(id_t const id, graph_t &graph);

    inline bool operator==(iterator const &b);
    inline bool operator!=(iterator const &b);

    inline pointer operator->();
    inline reference operator*();
    inline iterator& operator++();
    
};

class graph_t::node_t::iterator : protected graph_t::node_t, public std::iterator<
                        std::random_access_iterator_tag,    // iterator_category
                        node_t,                             // value_type
                        id_t,                               // difference_type
                        edge_t *,                            // pointer
                        edge_t                            // reference
> {
    friend graph_t;
    using graph_t::node_t::id;
    index_t edge_id;
public:

    node_t &operator[](id_t const) = delete;

    inline explicit iterator(index_t const id, node_t &node);

    inline bool operator==(iterator const &b) const;
    inline bool operator!=(iterator const &b) const;

    inline pointer operator->();
    inline reference operator*();
    inline iterator& operator++(); 
};

using node_t = graph_t::node_t;

/*inline node_t const graph_t::operator[](id_t const id) const {
//   return node_t(id, *this);
//    return vertex[id];
}*/

inline node_t graph_t::operator[](id_t const id) {
    return node_t{id, *this};
//TEST    return vertex[id];
}

/*inline node_t const graph_t::operator[](node_idx_t const id) const {
    return operator[](id.id);
}*/

inline node_t graph_t::operator[](node_idx_t const id) {
    return operator[](id.id);
}

inline graph_t::iterator graph_t::begin() {
    return iterator{0, *this};
}

inline graph_t::iterator graph_t::end() {
    return iterator{number_of_nodes(), *this};
}

inline id_t graph_t::number_of_nodes() {
    return vicinity.size();
}

inline index_t graph_t::number_of_edges() {
    return weight.size() / 2;
}

inline graph_t::edge_t graph_t::edge_of(node_idx_t node_id, index_t edge_id) {
    return edge_t{ vicinity[node_id.id][edge_id] };
}

inline void graph_t::add_terminal(node_idx_t const id) {
    terminal.emplace_back(id);
}

inline bool graph_t::is_terminal(node_idx_t const id) {
    return std::binary_search(std::begin(terminal), std::end(terminal), id);
}

inline id_t graph_t::number_of_terminals() {
    return terminal.size();
}

inline node_idx_t graph_t::ith_terminal(id_t const idx) {
    return terminal[idx];
}

inline std::vector<node_idx_t> const &graph_t::terminal_list() {
    return terminal;
}


inline void graph_t::add_edge(node_idx_t const v, node_idx_t const w, weight_t const weight) {
    vicinity[v.id].push_back( graph_t::weight.size() );
    neighbour.push_back( w.id );
    this->weight.push_back(weight);

    vicinity[w.id].push_back( graph_t::weight.size() );
    neighbour.push_back(v.id);
    this->weight.push_back(weight);
}

inline graph_t::edge_t::edge_t(index_t const id) : id(id) {
}

inline weight_t graph_t::edge_t::weight() const {
    return graph_t::weight[id];
}

inline node_idx_t graph_t::edge_t::dest() const {
    return {graph_t::neighbour[id]};
}

inline std::tuple<weight_t, node_idx_t> graph_t::edge_t::get() const {
    return std::make_tuple(weight(), dest());
}

inline bool graph_t::edge_t::operator==(edge_t const &b) const {
    return id == b.id;
}

inline bool graph_t::edge_t::operator!=(edge_t const &b) const {
    return !(*this == b);
}

inline index_t node_t::size() const {
    return graph.vicinity[id].size();
}

inline weight_t node_t::weight_of(id_t const pos) const {
    // assert pos < size()
    return graph_t::weight[graph.vicinity[id][pos]];
}

inline value_t &graph_t::node_t::value() {
    return graph.value[id];
}

inline node_t node_t::dest_of(id_t const pos) {
    // assert pos < size()
    return {graph_t::neighbour[graph.vicinity[id][pos]], graph};
}

inline node_idx_t graph_t::node_t::get_id() const {
    return {id};
}

inline graph_t::edge_t node_t::operator[](id_t const pos) const {
    return graph.vicinity[id][pos];
}

inline bool node_t::operator==(node_t const &node) const {
    return id == node.id;
}

inline bool node_t::operator!=(node_t const &node) const {
    return !(this->operator==(node));
}

inline graph_t::node_t::iterator graph_t::node_t::end() {
    return iterator(size(), *this);
}

inline graph_t::node_t::iterator graph_t::node_t::begin() {
    return iterator{0, *this};
}

// graph_t::iterator

inline graph_t::iterator::iterator(id_t const id, graph_t &graph) : node_t(id, graph) {
}

inline bool graph_t::iterator::operator==(iterator const &b){
    return id == b.id;
}

inline bool graph_t::iterator::operator!=(iterator const &b){
    return id != b.id;
}

inline graph_t::iterator::pointer graph_t::iterator::operator->() {
    return this;
}

inline graph_t::iterator::reference graph_t::iterator::operator*() {
    return *this;
}

inline graph_t::iterator& graph_t::iterator::operator++() {
    id+=1;

    return *this;
}

// graph_t::node_t::iterator

inline graph_t::node_t::iterator::iterator(index_t const edge_id, node_t &node) : node_t(node), edge_id(edge_id) {
}

inline bool graph_t::node_t::iterator::operator==(iterator const &b) const {
    return edge_id == b.edge_id && static_cast<node_t const &>(*this) == static_cast<node_t const &>(b);
}

inline bool graph_t::node_t::iterator::operator!=(iterator const &b) const {
    return !(*this == b);
}

inline graph_t::node_t::iterator::pointer graph_t::node_t::iterator::operator->() {
    static edge_t ret_edge(-1);
    ret_edge = **this; // TODO: ??? NO MULI THREAD!
    // SHOULD IT STAY LIKE THIS?

    return &ret_edge;
}

inline graph_t::node_t::iterator::reference graph_t::node_t::iterator::operator*() {
    return edge_t( static_cast<node_t &>(*this)[edge_id] );
}

inline graph_t::node_t::iterator &graph_t::node_t::iterator::operator++() {
    edge_id += 1;

    return *this;
}

// Reading

template<typename InputStreamT>
void graph_t::read_graph(InputStreamT &in) {
    // Clear graph
    // Start reading
    std::string instruction;

    // Read number of nodes
    id_t graph_size;
    while(instruction != "Nodes") {
        in >> instruction;
    }
    in >> graph_size;

    // Read number of edges
    in >> instruction;
    if(instruction != "Edges") {
        // TODO: throw  ...
    }

    index_t number_of_edges;
    in >> number_of_edges;

    weight.reserve(weight.size() + number_of_edges * 2);
    neighbour.reserve(neighbour.size() + number_of_edges * 2);

    // Read graph
    id_t p, q;
    weight_t w;

    std::vector<std::tuple<id_t, id_t, weight_t>> input_graph;
    input_graph.reserve(number_of_edges * 2);

    in >> instruction;
    for(; instruction != "END"; in >> instruction) {
        in >> p >> q >> w;
        p -= 1; q -= 1; // as p, q from 1 to n insted of 0 to n-1.
        input_graph.emplace_back(p, q, w);
        input_graph.emplace_back(q, p, w);
    }

    std::sort(std::begin(input_graph), std::end(input_graph));

    vicinity.resize(graph_size);

    for(auto const &edge : input_graph) {
        vicinity[std::get<0>(edge)].push_back(weight.size());

        weight.push_back(std::get<2>(edge));
        neighbour.push_back(std::get<1>(edge));
    }

    while(instruction != "Terminals") {
        while (instruction != "SECTION") {
            in >> instruction;
        }
        in >> instruction;
    }
    in >> instruction;
    while(instruction != "Terminals") {
        in >> instruction;
    }

    id_t number_of_terminals;
    in >> number_of_terminals;

    for(uint32_t p, i = 0; i < number_of_terminals; ++i) {
        while(instruction != "T") {
            in >> instruction;
        }
        in >> p;
        p -= 1;
        in >> instruction;
        terminal.push_back( node_idx_t{p} );
    }

    std::sort(std::begin(terminal), std::end(terminal));
}




#endif // G_GRAPH_H
