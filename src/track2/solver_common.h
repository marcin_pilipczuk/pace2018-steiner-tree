#include <algorithm>
#include <vector>
#include <memory>
#include <set>
#include <map>
#include <functional>
#include <string>

#include "treewidth.h"

#ifndef HC_SOLVER_COMMON

#define HC_SOLVER_COMMON

struct Edge {
	std::pair<int,int> edge;
	std::shared_ptr<Edge> previous;

	Edge();
	Edge(std::pair<int,int> const & edge, std::shared_ptr<Edge> pv);
};

#define PS_SIZE 25

struct PartialSolution {
	char component[PS_SIZE];
	std::shared_ptr<Edge> edges;
	size_t cost;

	PartialSolution(std::shared_ptr<Edge>,std::size_t _cost);
	PartialSolution(std::shared_ptr<Edge>);
	PartialSolution();
};

#define TWO_EDGES 63

namespace std {
	template<>
	struct hash<PartialSolution> {
		size_t operator() (PartialSolution const & ps) const; 
	};
}

bool operator==(PartialSolution const & A, PartialSolution const & B);
bool operator!=(PartialSolution const & A, PartialSolution const & B);
bool operator<(PartialSolution const & A, PartialSolution const & B);

//
// Solver class
//

typedef std::vector<PartialSolution> PartialSolutions;

class FindUnion {
private:
	std::vector<int> par;
public:
	void clear();
	FindUnion(int n);

	int parent(int e);
	void join(int e1, int e2);
};

class Solver {
private:
	std::size_t result;
	std::shared_ptr<Edge> result_edges;
	std::function<PartialSolutions(PartialSolutions,int)> reduction;
	PartialSolutions process_node(int root, std::vector<TWNode> const & nodes);
public:
	Solver(std::function<PartialSolutions(PartialSolutions,int)>);
	std::pair<size_t,EdgeSet> get(NiceDecomposition & nodes);
};

//
// Getting partial solutions divided into buckets
//

std::size_t getBucket(char const * match, int k);
std::unordered_map<std::size_t,PartialSolutions> getBuckets(PartialSolutions solutions, int k);
PartialSolutions simpleReduction(PartialSolutions result);

PartialSolutions reduction(PartialSolutions solutions, int k);

#endif

