#include "strange_io.h"
#include "treewidth.h"
#include "solver_common.h"

using namespace std;

int main() {
	[[maybe_unused]]auto [ graph, edge_map, terminals, ordinary_decomposition ] = readInput(cin);

	auto decomposition = makeDecompositionNice(ordinary_decomposition,edge_map,terminals);

	auto solver = Solver(reduction);
	auto [ cost, edges ] = solver.get(decomposition);
	
	// cost check
	
	FindUnion FU(graph.size());
	size_t real_cost = 0l;
	for (auto e : edges) {
		real_cost += edge_map[e];
		FU.join(e.first,e.second);
	}

	if (real_cost != cost) {
		printf("Sum of edge costs and computed cost differs.\n");
		return 0;
	}

	auto first_parent = FU.parent(*(terminals.begin()));
	for (auto v : terminals)
		if(FU.parent(v) != first_parent) {
			printf("Terminals %d and %d are in different components.\n",*(terminals.begin()),v);
			return 0;
		}
	printf("Everything seems to be ok.\n");
}
