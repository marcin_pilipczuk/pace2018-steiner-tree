#include <cstdio>
#include <vector>
#include <set>
#include <algorithm>
#include <queue>
#include <map>
#include <cassert>
#include "treewidth.h"
#include "basic.h"
#include "strange_io.h"

//#define DEBUG

using namespace std;

//
// Ordinary decomposition constructors
//

OrdinaryDecomposition::OrdinaryDecomposition() {}
OrdinaryDecomposition::OrdinaryDecomposition(vector<vector<int>> && _vertices, vector<vector<int>> && _graph) :
	vertices(_vertices),
	graph(_graph)
{}

//
// Constructors
//

TWNode::TWNode(int _parent, vector<int> _children, vector<int> _vertices) :
	parent(_parent),
	vertices(_vertices),
	children(_children),
	type(NONE),
	mode(AVAILABLE)
{}

TWNode::TWNode(int _parent, vector<int> _vertices, vector<int> _children, NodeType _type) :
	parent(_parent),
	vertices(_vertices),
	children(_children),
	type(_type),
	mode(AVAILABLE),
	is_final(false)
{}

TWNode::TWNode(int _parent, vector<int> _vertices, vector<int> _children, NodeType _type, int _v) :
	parent(_parent),
	vertices(_vertices),
	children(_children),
	type(_type),
	mode(AVAILABLE),
	v(_v),
	is_final(false)
{}

TWNode::TWNode(int _parent, vector<int> _vertices, vector<int> _children, set<pair<int,int>> _Edges, NodeType _type, int _v, int _w, int _cost) :
	parent(_parent),
	vertices(_vertices),
	children(_children),
	edges(_Edges),
	type(_type),
	mode(AVAILABLE),
	v(_v),
	w(_w),
	is_final(false),
	cost(_cost)
{}

//
// Getting number of element in vector
//

int getNum(vector<int> vec, int element) {
	for (int i = 0; i < ((int) vec.size()); i++)
		if (vec[i] == element)
			return i;
	return -1;
}

//
// Sort vertices
//

void sortVertices(vector<TWNode> & nodes) {
	for (auto & node : nodes) {
		sort(node.vertices.begin(), node.vertices.end());
		if (node.type == ADD_VERTEX)
			node.v = getNum(node.vertices, node.v);
		if (node.type == DEL_VERTEX)
			node.v = getNum(nodes[node.children[0]].vertices, node.v);
	}
}

//
// Adding leaves
//

void createLeaf(int parent, vector<TWNode> & nodes) {
	nodes[parent].children.push_back(nodes.size());
	nodes.push_back(TWNode(parent,vector<int>(),vector<int>(),LEAF));
}

void createLeaves(vector<TWNode> & nodes) {
	int initial_size = (int) nodes.size();
	for (int i = 0; i < initial_size; i++)
		if (nodes[i].children.size() == 0)
			createLeaf(i, nodes);
}

//
// Adding add/remove nodes
//

void createAddNode(int child, vector<TWNode> & nodes, int v) {
	int p = nodes[child].parent;

	for (int & chd : nodes[p].children)
		if (chd == child)
			chd = nodes.size();
	nodes[child].parent = nodes.size();

	vector<int> new_vertices(nodes[child].vertices);
	new_vertices.push_back(v);

	nodes.push_back(TWNode(p,new_vertices,vector<int>({child}),ADD_VERTEX,v));
}

void createDelNode(int child, vector<TWNode> & nodes, int v) {
	int p = nodes[child].parent;

	for (int & chd : nodes[p].children)
		if (chd == child)
			chd = nodes.size();
	nodes[child].parent = nodes.size();

	auto new_vertices = withoutElement(nodes[child].vertices, v);

	nodes.push_back(TWNode(p,new_vertices,vector<int>({child}),DEL_VERTEX,v));
}

void createAddAndRemoveNodes(vector<TWNode> & nodes) {
	for (int i = 1; i < ((int)nodes.size()); i++) 
		if (not isDiffEmpty(nodes[i].vertices, nodes[nodes[i].parent].vertices)) {
			int v = firstInDiff(nodes[i].vertices, nodes[nodes[i].parent].vertices);
			createDelNode(i, nodes, v);
		}

	for (int i = 1; i < ((int)nodes.size()); i++) 
		if (not isDiffEmpty(nodes[nodes[i].parent].vertices, nodes[i].vertices)) {
			int v = firstInDiff(nodes[nodes[i].parent].vertices, nodes[i].vertices);
			createAddNode(i, nodes, v);
		}
}

//
// Getting nice joins
//

void rebuildJoinNode(int node, vector<TWNode> & nodes) {
	nodes[node].type = JOIN;

	auto rest_of_children = nodes[node].children;
	auto vertices = nodes[node].vertices;

	int last = node;

	while (rest_of_children.size() > 2) {
		int current_child = rest_of_children.back();
		rest_of_children.pop_back();

		nodes[last].children = vector<int>({current_child,(int)nodes.size()});
		nodes[current_child].parent = last;

		nodes.push_back(TWNode(last,vertices,vector<int>(),JOIN));
		last = nodes.size()-1;
	}

	nodes[last].children = rest_of_children;
	for (int child : rest_of_children)
		nodes[child].parent = last;
}

void rebuildJoinNodes(vector<TWNode> & nodes) {
	int initial_size = nodes.size();
	for (int i = 1; i < initial_size; i++)
		if (nodes[i].children.size() > 1)
			rebuildJoinNode(i, nodes);
}

//
// Adding "addEdge" nodes
//

void createAddEdgeNode(vector<TWNode> & nodes, int child, pair<int,int> e, int cost) {
	int p = nodes[child].parent;

	for (int & chd : nodes[p].children)
		if (chd == child)
			chd = nodes.size();
	nodes[child].parent = nodes.size();

	int v = getNum(nodes[child].vertices,e.first);
	int w = getNum(nodes[child].vertices,e.second);

	nodes.push_back(TWNode(p,nodes[child].vertices,vector<int>({child}),set<pair<int,int>>(),ADD_EDGE,v,w,cost));
}

void edgeClearing(vector<TWNode> & nodes, int root, set<pair<int,int>> const & edges_to_clear) {
	auto new_etc = edges_to_clear;
	auto old_edges = nodes[root].edges;

	for (auto e : old_edges)
		if (edges_to_clear.count(e) != 0)
			nodes[root].edges.erase(e);
		else
			new_etc.insert(e);

	for (auto child : nodes[root].children)
		edgeClearing(nodes, child, new_etc);
}

void createAddEdgeNodes(vector<TWNode> & nodes, EdgeMap const & edges) {
	for (auto & node : nodes) {
		for (int v : node.vertices) 
			for (int w : node.vertices)
				if (v < w and edges.count(edge(v,w)))
					node.edges.insert(edge(v,w));
	}

	edgeClearing(nodes,0,set<pair<int,int>>());
	
	for (int i = 1; i < ((int)nodes.size()); i++) {
		map<pair<int,int>,int> current_edges;
		for (auto edge : nodes[i].edges)
			current_edges[edge] = edges.at(edge);

		nodes[i].edges = set<pair<int,int>>();

		for (auto p : current_edges)
			createAddEdgeNode(nodes,i,p.first,p.second);
	}
}

//
// Cleaning NONEs
//

void removeNoneNodes(vector<TWNode> & nodes) {
	for (int i = 0; i < ((int)nodes.size()); i++) {
		while (nodes[i].parent != -1 and nodes[nodes[i].parent].type == NONE and nodes[nodes[i].parent].parent != nodes[i].parent)
			nodes[i].parent = nodes[nodes[i].parent].parent;
		for (int & child : nodes[i].children)
			while (nodes[child].type == NONE)
				child = nodes[child].children[0];
	}
	
	vector<int> new_nums(nodes.size());
	vector<TWNode> result;

	int current_num = 0;

	for (int i = 0; i < ((int)nodes.size()); i++)
		if (nodes[i].type != NONE)
			new_nums[i] = current_num++;

	for (int i = 0; i < ((int)nodes.size()); i++) if (nodes[i].type != NONE) {
		if (nodes[i].parent != -1)
			nodes[i].parent = new_nums[nodes[i].parent];
		for (int & child : nodes[i].children)
			child = new_nums[child];
		result.push_back(nodes[i]);
	}

	nodes = result;
}

//
// GraphToTree
//

void graphToTree(OrdinaryDecomposition & decomposition, int v, int parent, vector<int> & num, vector<TWNode> & result) {
	num[v] = result.size();

	result.push_back(TWNode(num[parent], vector<int>(), decomposition.vertices[v]));

	for (int w : decomposition.graph[v])
		if (w != parent) {
			graphToTree(decomposition,w,v,num,result);
			result[num[v]].children.push_back(num[w]);
		}
}

vector<TWNode> graphToTree(OrdinaryDecomposition decomposition) {
	int root = 1;
	for (int node = 1; node < (int)decomposition.graph.size(); node++) {
		if (decomposition.graph[node].size() == 1)
			root = node;
	}

	vector<TWNode> result({TWNode(-1,vector<int>({1}),vector<int>())});
	std::vector<int> num(decomposition.graph.size());
	num[0] = 0;
	graphToTree(decomposition,root,0,num,result);

	return result;
}

//
// Sort decomposition
//

int findRoot(vector<TWNode> const & nodes) {
	for (int i = 0; i < ((int) nodes.size()); i++)
		if (nodes[i].parent == -1)
			return i;
	return -1;
}

void sortDecomposition(vector<TWNode> const & nodes, int root, vector<TWNode> & result, vector<int> & new_num) {
	new_num[root] = result.size();
	result.push_back(nodes[root]);

	for (int child : nodes[root].children)
		sortDecomposition(nodes, child, result, new_num);
}

void sortDecomposition(vector<TWNode> & nodes) {
	vector<TWNode> result;
	vector<int> new_num(nodes.size()); 

	int root = findRoot(nodes);

	sortDecomposition(nodes, root, result, new_num);

	for (auto & node : result) {
		if (node.parent != -1)
			node.parent = new_num[node.parent];
		for (int & child : node.children)
			child = new_num[child];
	}

	nodes = result;
}

//
// The decomposition
//

OrdinaryDecomposition reduceStupidBranches(OrdinaryDecomposition decomposition) {
	int n = decomposition.graph.size()-1;

	for (int node = 1; node <= n; node++)
		sort(decomposition.vertices[node].begin(),decomposition.vertices[node].end());

	vector<bool> deleted(n+1,false);
	vector<int> deg(n+1,1);
	queue<int> leaves;

	for (int node = 1; node <= n; node++)
		if (decomposition.graph[node].size() == 1)
			leaves.push(node);
		else
			deg[node] = (int) decomposition.graph[node].size();

	int nodes_left = n;
	while (not leaves.empty() and nodes_left > 1) {
		int node = leaves.front();
		leaves.pop();

		for (int neigh : decomposition.graph[node])
			if (not deleted[neigh] and isDiffEmpty(decomposition.vertices[node],decomposition.vertices[neigh])) {
				deleted[node] = true;
				deg[neigh]--;

				if (deg[neigh] == 1)
					leaves.push(neigh);

				nodes_left--;
			}
	}

	OrdinaryDecomposition result;

	// get new nums

	vector<int> new_num(n+1);

	result.vertices.push_back(vector<int>());
	for (int node = 1; node <= n; node++)
		if (not deleted[node]) {
			new_num[node] = result.vertices.size();
			result.vertices.push_back(decomposition.vertices[node]);
		}

	// clean graph

	result.graph.push_back(vector<int>());
	for (int node = 1; node <= n; node++)
		if (not deleted[node]) {
			vector<int> new_edges;
			for (int neigh : decomposition.graph[node])
				if (not deleted[neigh])
					new_edges.push_back(new_num[neigh]);
			result.graph.push_back(new_edges);
		}

	return result;
}

//
// Mark terminals
//

void markTerminals(NiceDecomposition & decomposition, VertexSet const & terminals) {
	for (auto & node : decomposition) {
		if (node.type == ADD_VERTEX and terminals.count(node.vertices[node.v]))
			node.mode = TERMINAL;
		if (node.type == DEL_VERTEX and terminals.count(decomposition[node.children[0]].vertices[node.v]))
			node.mode = TERMINAL;
	}
}

//
// Nodes which can be final
//

bool hasTerminals(NiceDecomposition & nodes, int root) {
	if (nodes[root].type == DEL_VERTEX and nodes[root].mode == TERMINAL)
		return true;

	bool result = false;
	for (auto child : nodes[root].children)
		result = result or hasTerminals(nodes,child);

	return result;
}

void markFinal(NiceDecomposition & nodes, int root) {
	nodes[root].is_final = true;

	if (nodes[root].type == DEL_VERTEX and nodes[root].mode == TERMINAL)
		return;
	if (nodes[root].type == JOIN and hasTerminals(nodes,nodes[root].children[0]) and hasTerminals(nodes,nodes[root].children[1]))
		return;

	for (int child : nodes[root].children)
		markFinal(nodes, child);
}

//
// Make decomposition nice
//

NiceDecomposition makeDecompositionNice(OrdinaryDecomposition decomposition, EdgeMap const & edges, VertexSet const & terminals) {
	auto result = graphToTree(reduceStupidBranches(decomposition));

	createLeaves(result);
	createAddAndRemoveNodes(result);
	sortVertices(result);
	rebuildJoinNodes(result);
	createAddEdgeNodes(result,edges);
	removeNoneNodes(result);
	sortDecomposition(result);
	markTerminals(result,terminals);
	markFinal(result,0);
	
	return result;
}

bool operator==(TWNode const & A, TWNode const & B) {
	if (A.type != B.type)
		return false;
	return true;
}

void setVertexModes(NiceDecomposition & decomposition, VertexSet const & deleted, VertexSet const & chosen) {
	for (auto & node : decomposition)
		if (node.type == ADD_VERTEX and node.mode != TERMINAL) {
			int v = node.vertices[node.v];

			if (deleted.count(v) != 0)
				node.mode = DELETED;
			else if (chosen.count(v) != 0)
				node.mode = CHOSEN;
			else
				node.mode = AVAILABLE;
		}
}

void setEdgeModes(NiceDecomposition & decomposition, EdgeSet const & deleted, EdgeSet const& chosen) {
	for (auto & node : decomposition)
		if (node.type == ADD_VERTEX and node.mode != TERMINAL) {
			int v = node.vertices[node.v];
			int w = node.vertices[node.w];

			if (chosen.count(edge(v,w)) != 0)
				node.mode = CHOSEN;
			else if (deleted.count(edge(v,w)))
				node.mode = DELETED;
			else
				node.mode = AVAILABLE;
		}
}

void markExactEdges(NiceDecomposition & decomposition, EdgeSet const& chosen) {
	for (auto & node : decomposition)
		if (node.type == ADD_VERTEX and node.mode != TERMINAL) {
			int v = node.vertices[node.v];
			int w = node.vertices[node.w];

			if (chosen.count(edge(v,w)) != 0)
				node.mode = CHOSEN;
			else
				node.mode = DELETED;
		}
}

