#include "strange_io.h"
#include "treewidth.h"
#include "solver_common.h"

using namespace std;

int main() {
	[[maybe_unused]] auto [ _, edge_map, terminals, ordinary_decomposition, chosen_edges ] = readInputWithChosenEdges(cin);

	auto decomposition = makeDecompositionNice(ordinary_decomposition,edge_map,terminals);
	markExactEdges(decomposition,chosen_edges);

	auto solver = Solver(reduction);
	auto [ cost, vertices ] = solver.get(decomposition);
	printOutput(cost,vertices);
}
