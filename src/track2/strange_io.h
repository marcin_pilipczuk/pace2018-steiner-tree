#include <algorithm>
#include <vector>
#include <iostream>

#include "treewidth.h"

#ifndef STRANGE_IO

#define STRANGE_IO

void printDecomposition(OrdinaryDecomposition decomposition, int n);
void printDecomposition(std::vector<TWNode> nodes, int n);
void printDecompositionDebug(std::vector<TWNode> nodes);

std::tuple<Graph,EdgeMap,std::set<int>,OrdinaryDecomposition> readInput(std::istream & in);
std::tuple<Graph,EdgeMap,std::set<int>,OrdinaryDecomposition,EdgeSet> readInputWithChosenEdges(std::istream & in);
void printOutput(size_t,EdgeSet const&);

#endif

