#include "basic.h"

bool checkIfSteinerTree(EdgeSet const & tree, Graph const & graph, VertexSet const & terminals);

