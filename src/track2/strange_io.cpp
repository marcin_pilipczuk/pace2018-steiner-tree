#include <cstdio>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <tuple>

#include "strange_io.h"
#include "basic.h"

using namespace std;

void printDecomposition(OrdinaryDecomposition decomposition, int n) {

	int width = 0;
	for (auto const & bucket : decomposition.vertices)
		width = max((int)bucket.size(),width);

	int num = decomposition.vertices.size()-1;

	printf("s td %d %d %d\n", num, width, n);

	for (int i = 1; i <= num; i++) {
		printf("b %d", i);
		for (int v : decomposition.vertices[i]) {
			printf(" %d", v);
		}
		printf("\n");
	}

	for (int node = 1; node <= num; node++)
		for (int neigh : decomposition.graph[node])
			if (neigh < node)
				printf("%d %d\n", node, neigh);
}

void printDecomposition(vector<TWNode> nodes, int n) {

	int width = 0;
	for (auto node : nodes) {
		if (width < ((int) node.vertices.size()))
			width = node.vertices.size();
	}

	int num = nodes.size();

	printf("s td %d %d %d\n", num, width, n);

	for (int i = 0; i < num; i++) {
		printf("b %d", i+1);
		for (int v : nodes[i].vertices) {
			printf(" %d", v);
		}
		printf("\n");
	}

	for (int i = 1; i < num; i++) {
		printf("%d %d\n", i+1, nodes[i].parent+1);
	}
}

void printDecompositionDebug(vector<TWNode> nodes) {
	char nodeTypeStr[6][20] = {"LEAF", "ADD_VERTEX", "ADD_EDGE", "DEL_VERTEX", "JOIN", "NONE"};

	for (int i = 0; i < ((int) nodes.size()); i++) {
		printf("NODE: %d ", i);
		printf("TYPE: %s ", nodeTypeStr[nodes[i].type]);
		printf("PARENT: %d ", nodes[i].parent);
		printf("VERTICES:");
		for (int v : nodes[i].vertices)
			printf(" %d", v);
		printf(" EDGES:");
		for (auto p : nodes[i].edges)
			printf(" (%d,%d) ", p.first, p.second);
		printf("CHILDREN:");
		for (int child : nodes[i].children)
			printf(" %d", child);
		if (nodes[i].type == ADD_VERTEX or nodes[i].type == DEL_VERTEX or nodes[i].type == ADD_EDGE)
			printf(" V: %d", nodes[i].v);
		if (nodes[i].type == ADD_EDGE)
			printf(" W: %d COST: %d", nodes[i].w, nodes[i].cost);
		if (nodes[i].mode == TERMINAL)
			printf(" TERMINAL");
		if (nodes[i].is_final)
			printf(" FINAL");
		printf("\n");
	}
}

vector<int> getNumsFromLine(string & line, int pos) {
	vector<int> result;

	int num;
	bool actualNum = false;

	for (char c : line)
		if ('0' <= c and '9' >= c)
			if (not actualNum) {
				actualNum = true;
				num = c - '0';
			} else {
				num *= 10;
				num += c - '0';
			}
		else
			if (actualNum) {
				result.push_back(num);
				actualNum = false;
			}

	if (actualNum)
		result.push_back(num);

	return result;
}

tuple<Graph,EdgeMap,VertexSet,OrdinaryDecomposition> readInput(istream & in) {
    string instruction,the_void,line;

    size_t n,m;
    while(instruction != "Nodes")
        in >> instruction;
    in >> n;
    in >> the_void;
    in >> m;

	Graph graph(n+1);
	EdgeMap edge_map;

	for (size_t i = 1l; i <= m; i++) {
		int A, B, w;
		in >> the_void >> A >> B >> w;

		graph[A].push_back(B);
		graph[B].push_back(A);

		edge_map[edge(A,B)] = w;
	}

	in >> the_void >> the_void >> the_void >> the_void;

    size_t number_of_terminals;
    in >> number_of_terminals;

	VertexSet terminals;
	for (size_t i = 1l; i <= number_of_terminals; i++) {
		int terminal;
		cin >> the_void >> terminal;
		terminals.insert(terminal);
	}

	cin >> the_void >> the_void >> the_void >> the_void;

	getline(in,line);
	getline(in,line);

	OrdinaryDecomposition decomposition;
	vector<int> nums;
	int bag_num;

	while (line.compare(0,3,"END")) {
		switch (line[0]) {
			case 'c' :
				break;
			case 's' :
				nums = getNumsFromLine(line,5);
				decomposition.vertices = vector<vector<int>>(nums[0]+1);
				decomposition.graph = Graph(nums[0]+1);
				break;
			case 'b' :
				nums = getNumsFromLine(line,2);
				bag_num = nums.front();
				nums.erase(nums.begin());
				decomposition.vertices[bag_num].swap(nums);
				break;
			default :
				nums = getNumsFromLine(line,0);
				decomposition.graph[nums[0]].push_back(nums[1]);
				decomposition.graph[nums[1]].push_back(nums[0]);
		}

		getline(in,line);
	}

	return make_tuple(graph,edge_map,terminals,decomposition);
}

/*tuple<Graph,EdgeMap,VertexSet,OrdinaryDecomposition,size_t,EdgeSet> readInputWithAnswer(istream & in) {
    string instruction,the_void,line;

    size_t n,m;
    while(instruction != "Nodes")
        in >> instruction;
    in >> n;
    in >> the_void;
    in >> m;

	Graph graph(n+1);
	EdgeMap edge_map;

	for (size_t i = 1l; i <= m; i++) {
		int A, B, w;
		in >> the_void >> A >> B >> w;

		graph[A].push_back(B);
		graph[B].push_back(A);

		edge_map[edge(A,B)] = w;
	}

	in >> the_void >> the_void >> the_void >> the_void;

    size_t number_of_terminals;
    in >> number_of_terminals;

	VertexSet terminals;
	for (size_t i = 1l; i <= number_of_terminals; i++) {
		int terminal;
		cin >> the_void >> terminal;
		terminals.insert(terminal);
	}

	cin >> the_void >> the_void >> the_void >> the_void;

	getline(in,line);
	getline(in,line);

	OrdinaryDecomposition decomposition;
	vector<int> nums;
	int bag_num;

	while (line.compare(0,3,"END")) {
		switch (line[0]) {
			case 'c' :
				break;
			case 's' :
				nums = getNumsFromLine(line,5);
				decomposition.vertices = vector<vector<int>>(nums[0]+1);
				decomposition.graph = Graph(nums[0]+1);
				break;
			case 'b' :
				nums = getNumsFromLine(line,2);
				bag_num = nums.front();
				nums.erase(nums.begin());
				decomposition.vertices[bag_num].swap(nums);
				break;
			default :
				nums = getNumsFromLine(line,0);
				decomposition.graph[nums[0]].push_back(nums[1]);
				decomposition.graph[nums[1]].push_back(nums[0]);
		}

		getline(in,line);
	}

	cin >> sth;
	if (sth != value)
		coś jest nie tak;

	return make_tuple(graph,edge_map,terminals,decomposition);
}*/

void printOutput(size_t cost, EdgeSet const & edges) {
	cout << "VALUE " << cost << endl;
	for (auto e : edges)
		cout << e.first << " " << e.second << endl;
}
