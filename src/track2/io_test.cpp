#include "basic.h"
#include "treewidth.h"
#include "strange_io.h"

int main() {
	auto [ graph, edge_map, terminals, decomposition ] = readInput(std::cin);

	std::cout << "NUMBER OF VERTICES: " << graph.size()-1 << std::endl;
	std::cout << "NEIGHBOURS:" << std::endl;
	for (int v = 1; v < (int)graph.size(); v++) {
		if (terminals.count(v) == 0)
			std::cout << "C " << v << ":";
		else 
			std::cout << "T " << v << ":";
		for (int w : graph[v])
			std::cout << " " << w;
		std::cout << std::endl;	
	}

	std::cout << std::endl << "NUMBER OF EDGES: " << edge_map.size() << std::endl;
	std::cout << "WEIGHTS:" << std::endl;
	for (auto p : edge_map)
		std::cout << "(" << p.first.first << "," << p.first.second << ") : " << p.second << std::endl; 

	std::cout << "DECOMPOSITION:" << std::endl;
	printDecompositionDebug(makeDecompositionNice(decomposition,edge_map,terminals));
}
