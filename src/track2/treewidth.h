#include <vector>
#include <algorithm>
#include <set>

#include "basic.h"

#ifndef TWNODE

#define TWNODE

struct OrdinaryDecomposition {
	std::vector<std::vector<int>> vertices;
	std::vector<std::vector<int>> graph;

	OrdinaryDecomposition();
	OrdinaryDecomposition(std::vector<std::vector<int>>&&,std::vector<std::vector<int>>&&);
};

enum NodeType {
	LEAF,
	ADD_VERTEX,
	ADD_EDGE,
	DEL_VERTEX,
	JOIN,
	NONE
};

enum VertexMode {
	TERMINAL,
	CHOSEN,
	AVAILABLE,
	DELETED
};

struct TWNode {
	int parent;
	std::vector<int> vertices;
	std::vector<int> children;
	std::set<std::pair<int,int>> edges;
	NodeType type;
	VertexMode mode = AVAILABLE;
	int v;
	int w;
	bool is_final = false;

	bool marked = false;

	int cost;

	TWNode(int,std::vector<int>,std::vector<int>);
	TWNode(int,std::vector<int>,std::vector<int>,NodeType);
	TWNode(int,std::vector<int>,std::vector<int>,NodeType,int);
	TWNode(int,std::vector<int>,std::vector<int>,std::set<std::pair<int,int>>,NodeType,int,int,int);
};

typedef std::vector<TWNode> NiceDecomposition;

bool operator==(TWNode const & A, TWNode const & B);

OrdinaryDecomposition decompose(int n, Graph, EdgeSet);

NiceDecomposition makeDecompositionNice(OrdinaryDecomposition,EdgeMap const &,VertexSet const &);

void setVertexModes(NiceDecomposition &, VertexSet const &, VertexSet const&);
void setEdgeModes(NiceDecomposition &, EdgeSet const &, EdgeSet const&);
void markExactEdges(NiceDecomposition &, EdgeSet const&);

#endif

