#include <queue>

#include "utils.h"

using namespace std;

bool checkIfSteinerTree(EdgeSet const & tree, Graph const & graph, VertexSet const & terminals) {
	Graph graph_tree(graph.size());
	auto first_terminal = *(terminals.begin());

	for (auto e : tree) {
		graph_tree[e.first].push_back(e.second);
		graph_tree[e.second].push_back(e.first);
	}

	queue<int> Q;
	vector<bool> visited(graph.size());

	Q.push(first_terminal);
	visited[first_terminal] = true;

	while (not Q.empty()) {
		int v = Q.front();
		Q.pop();

		for (int w : graph_tree[v])
			if (not visited[w]) {
				visited[w] = true;
				Q.push(w);
			}
	}

	for (auto terminal : terminals)
		if (not visited[terminal])
			return false;
	return true;
}
