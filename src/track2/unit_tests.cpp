#include <gtest/gtest.h>
#include <vector>
#include <set>
#include <functional>
#include <unordered_map>
#include <iostream>
#include <bitset>

#include "basic.h"
#include "treewidth.h"
#include "solver_common.h"
#include "gaussian.h"

using namespace std;

//
// ADD VERTEX tests
//

void addVertex(TWNode const & node, PartialSolutions & solutions);

TEST(AddVertex, casualBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6,7}), vector<int>({1}), ADD_VERTEX, 0);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,67,68,67,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];

	addVertex(node,solutions);

	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{0,1,68,69,68,69,6};
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(AddVertex, casualEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6,7}), vector<int>({1}), ADD_VERTEX, 6);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,67,68,67,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];

	addVertex(node,solutions);

	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,67,68,67,68,5,6};
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(AddVertex, casualMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6,7}), vector<int>({1}), ADD_VERTEX, 3);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,67,68,67,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];

	addVertex(node,solutions);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,68,69,3,68,69,6};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(AddVertex, terminalBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6,7}), vector<int>({1}), ADD_VERTEX, 0);
	node.mode = TERMINAL;
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,67,68,67,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];

	addVertex(node,solutions);

	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,1,68,69,68,69,6};
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(AddVertex, terminalEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6,7}), vector<int>({1}), ADD_VERTEX, 6);
	node.mode = TERMINAL;
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,67,68,67,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];

	addVertex(node,solutions);

	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,67,68,67,68,5,70};
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(AddVertex, terminalMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6,7}), vector<int>({1}), ADD_VERTEX, 3);
	node.mode = TERMINAL;
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,67,68,67,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];

	addVertex(node,solutions);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,68,69,67,68,69,6};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

//
// DEL VERTEX tests
//

PartialSolutions delVertex(TWNode const & node, PartialSolutions & solutions, size_t & result, shared_ptr<Edge> & result_edge);

TEST(DelVertex, casualBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 0);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,4,69,67,4,69,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{3,68,66,3,68,5};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(DelVertex, casualEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 6);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,68,69,67,68,69,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,68,69,67,68,69};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(DelVertex, casualMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 3);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,68,69,3,68,69,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,67,68,67,68,5};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(DelVertex, terminalGoodBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 0);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{69,68,69,67,68,69,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{67,68,66,67,68,5};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(DelVertex, terminalGoodEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 6);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,68,70,67,68,70,70};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,68,69,67,68,69};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(DelVertex, terminalGoodMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 3);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{67,68,69,67,68,69,69};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{64,67,68,67,68,68};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
}

TEST(DelVertex, terminalBadBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 0);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{64,68,69,67,68,69,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
}

TEST(DelVertex, terminalBadEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 6);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{67,68,68,67,68,5,70};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
}

TEST(DelVertex, terminalBadMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 3);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,68,69,67,68,69,70};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = -1;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
}

shared_ptr<Edge> core_edge(new Edge(mp(3,3),NULL));
size_t core_result = 100l;

TEST(DelVertex, finalGoodBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 0);
	node.is_final = true;
	PartialSolutions solutions({PartialSolution(core_edge,core_result)});

	vector<char> component{64,1,2,3,4,5,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = 10000l;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
	ASSERT_EQ(result,100l);
	ASSERT_EQ(result_edge->edge,mp(3,3));
}

TEST(DelVertex, finalGoodEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 6);
	node.is_final = true;
	PartialSolutions solutions({PartialSolution(core_edge,core_result)});

	vector<char> component{0,1,2,3,4,5,70};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = 10000l;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
	ASSERT_EQ(result,100l);
	ASSERT_EQ(result_edge->edge,mp(3,3));
}

TEST(DelVertex, finalGoodMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 3);
	node.is_final = true;
	PartialSolutions solutions({PartialSolution(core_edge,core_result)});

	vector<char> component{0,1,2,67,4,5,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = 10000l;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
	ASSERT_EQ(result,100l);
	ASSERT_EQ(result_edge->edge,mp(3,3));
}

TEST(DelVertex, finalBadBegin) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 3);
	node.is_final = true;
	PartialSolutions solutions({PartialSolution(core_edge,core_result)});

	vector<char> component{0,65,2,67,4,5,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = 10000l;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
	ASSERT_EQ(result,10000l);
	ASSERT_EQ(result_edge->edge,mp(-2,-2));
}

TEST(DelVertex, finalBadEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 6);
	node.is_final = true;
	PartialSolutions solutions({PartialSolution(core_edge,core_result)});

	vector<char> component{64,1,2,3,68,5,70};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = 10000l;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
	ASSERT_EQ(result,10000l);
	ASSERT_EQ(result_edge->edge,mp(-2,-2));
}

TEST(DelVertex, finalBadMid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}), vector<int>({1}), DEL_VERTEX, 3);
	node.is_final = true;
	PartialSolutions solutions({PartialSolution(core_edge,core_result)});

	vector<char> component{0,1,2,67,4,69,6};
	for (int i = 0; i < 7; i++)
		solutions[0].component[i] = component[i];

	size_t result = 10000l;
	shared_ptr<Edge> result_edge(new Edge(mp(-2,-2),NULL));

	solutions = delVertex(node,solutions,result,result_edge);
	ASSERT_EQ(solutions.size(),0);
	ASSERT_EQ(result,10000l);
	ASSERT_EQ(result_edge->edge,mp(-2,-2));
}

//
// ADD EDGE tests
//

void addEdge(TWNode const & node, PartialSolutions & solutions);

TEST(AddEdge, casual) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}),vector<int>({1}),set<pair<int,int>>({}),ADD_EDGE,1,4,5l);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,1,2,67,4,69};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];
	solutions[0].cost=11l;

	addEdge(node,solutions);
	ASSERT_EQ(solutions.size(),2);

	vector<char> proper_component{0,4,2,67,4,69};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],component[i]);
	ASSERT_EQ(solutions[0].cost,11l);
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[1].component[i],proper_component[i]);
	ASSERT_EQ(solutions[1].cost,16l);
}

TEST(AddEdge, beginAndEnd) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}),vector<int>({1}),set<pair<int,int>>({}),ADD_EDGE,0,5,5l);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,1,2,67,4,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];
	solutions[0].cost=11l;

	addEdge(node,solutions);
	ASSERT_EQ(solutions.size(),2);

	vector<char> proper_component{5,1,2,67,4,5};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],component[i]);
	ASSERT_EQ(solutions[0].cost,11l);
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[1].component[i],proper_component[i]);
	ASSERT_EQ(solutions[1].cost,16l);
}

TEST(AddEdge, invalid) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}),vector<int>({1}),set<pair<int,int>>({}),ADD_EDGE,1,3,5l);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,69,2,69,68,69};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];
	solutions[0].cost=11l;

	addEdge(node,solutions);
	ASSERT_EQ(solutions.size(),1);

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],component[i]);
	ASSERT_EQ(solutions[0].cost,11l);
}

TEST(AddEdge, terminal) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}),vector<int>({1}),set<pair<int,int>>({}),ADD_EDGE,3,4,5l);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{0,1,2,67,4,69};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];
	solutions[0].cost=11l;

	addEdge(node,solutions);
	ASSERT_EQ(solutions.size(),2);

	vector<char> proper_component{0,1,2,68,68,69};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],component[i]);
	ASSERT_EQ(solutions[0].cost,11l);
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[1].component[i],proper_component[i]);
	ASSERT_EQ(solutions[1].cost,16l);
}

TEST(AddEdge, twoTerminals) {
	TWNode node(-1, vector<int>({1,2,3,4,5,6}),vector<int>({1}),set<pair<int,int>>({}),ADD_EDGE,1,3,5l);
	PartialSolutions solutions({PartialSolution()});

	vector<char> component{65,65,2,67,4,69};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component[i];
	solutions[0].cost=11l;

	addEdge(node,solutions);
	ASSERT_EQ(solutions.size(),2);

	vector<char> proper_component{67,67,2,67,4,69};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],component[i]);
	ASSERT_EQ(solutions[0].cost,11l);
	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[1].component[i],proper_component[i]);
	ASSERT_EQ(solutions[1].cost,16l);
}

//
// JOIN tests
//

PartialSolutions join(TWNode const &,PartialSolutions &&,PartialSolutions &&);

shared_ptr<Edge> join_core_edge(new Edge(mp(-1,-1),NULL));

TEST(Join, casual) {
	TWNode node(-1,vector<int>({1,2}),vector<int>({1,2,3,4,5,6}));

	PartialSolutions solutions_a({PartialSolution(join_core_edge,core_result)});
	vector<char> component_a{0,65,2,68,68,5};
	for (int i = 0; i < 6; i++)
		solutions_a[0].component[i] = component_a[i];
	solutions_a[0].cost=7l;

	PartialSolutions solutions_b({PartialSolution(join_core_edge,core_result)});
	vector<char> component_b{0,67,2,67,4,5};
	for (int i = 0; i < 6; i++)
		solutions_b[0].component[i] = component_b[i];
	solutions_b[0].cost=19l;

	auto solutions = join(node,move(solutions_a),move(solutions_b));
	ASSERT_EQ(solutions.size(),1);

	vector<char> proper_component{0,68,2,68,68,5};

	for (int i = 0; i < (int)node.vertices.size(); i++)
		ASSERT_EQ(solutions[0].component[i],proper_component[i]);
	ASSERT_EQ(solutions[0].cost,26l);
}

TEST(Join, fail) {
	TWNode node(-1,vector<int>({1,2}),vector<int>({1,2,3,4,5,6}));

	PartialSolutions solutions_a({PartialSolution(join_core_edge,core_result)});
	vector<char> component_a{0,65,2,70,70,70};
	for (int i = 0; i < 6; i++)
		solutions_a[0].component[i] = component_a[i];
	solutions_a[0].cost=7l;

	PartialSolutions solutions_b({PartialSolution(join_core_edge,core_result)});
	vector<char> component_b{0,67,2,67,4,67};
	for (int i = 0; i < 6; i++)
		solutions_b[0].component[i] = component_b[i];
	solutions_b[0].cost=19l;

	auto solutions = join(node,move(solutions_a),move(solutions_b));
	ASSERT_EQ(solutions.size(),0);
}

TEST(Join, multum) {
	TWNode node(-1,vector<int>({1,2}),vector<int>({1,2,3,4,5,6}));

	PartialSolutions solutions_a({
		PartialSolution(join_core_edge,core_result),
		PartialSolution(join_core_edge,core_result),
		PartialSolution(join_core_edge,core_result)
	});
	vector<char> component_a_1{0,65,2,70,70,70}, component_a_2{0,0,0,0,0,0}, component_a_3{0,0,0,0,0,0};
	for (int i = 0; i < 6; i++) {
		solutions_a[0].component[i] = component_a_1[i];
		solutions_a[1].component[i] = component_a_2[i];
		solutions_a[2].component[i] = component_a_3[i];
	}

	solutions_a[0].cost=7l;

	PartialSolutions solutions_b({PartialSolution(join_core_edge,core_result)});
	vector<char> component_b{0,67,2,67,4,67};
	for (int i = 0; i < 6; i++)
		solutions_b[0].component[i] = component_b[i];
	solutions_b[0].cost=19l;

	auto solutions = join(node,move(solutions_a),move(solutions_b));
	ASSERT_EQ(solutions.size(),0);
}

//
// Tests for simple reduction
//

/*
PartialSolutions simple_reduction(PartialSolutions result) {
	if (result.size() <= 1)
		return result;

	sort(result.begin(), result.end());

	auto first_free = result.begin();
	for (auto ptr = result.begin(); ptr != prev(result.end()); ptr++)
		if (*ptr != *next(ptr)) {
			*first_free = *ptr;
			first_free++;
		} else if (ptr->cost < next(ptr)->cost) {
			*(next(ptr)) = *ptr;
		}
	*first_free = *prev(result.end());
	first_free++;

	if (first_free != prev(result.end()))
		result.erase(first_free, result.end());

	return result;
}*/

TEST(SmallReduction, simple) {
	PartialSolutions solutions(4);
	vector<char> component_0{0,68,2,68,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component_0[i];
	solutions[0].cost=3l;
	vector<char> component_1{0,68,2,68,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component_1[i];
	solutions[1].cost=6l;
	vector<char> component_2{0,65,68,68,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component_2[i];
	solutions[2].cost=6l;
	vector<char> component_3{0,65,68,68,68,5};
	for (int i = 0; i < 6; i++)
		solutions[0].component[i] = component_3[i];
	solutions[3].cost=3l;

	solutions = simpleReduction(solutions);

	ASSERT_EQ(solutions.size(),2);
	ASSERT_EQ(solutions[0].cost,3l);
	ASSERT_EQ(solutions[1].cost,3l);
}

//
// Tests for reduction
//


bool ifDivisionMatch(set<char> const & div, char const * match, int k);
vector<set<char>> getDivisions(const char * match, int k);

TEST(GetDivisions, emptySet) {
	int k = 4;
	char match[4] = {0,1,2,3};
	auto result = getDivisions(match,k);
	ASSERT_EQ(result.size(),1);
	ASSERT_EQ(result[0].size(),0);
}

TEST(GetDivisions, fullSet) {
	int k = 4;
	char match[4] = {1,0,3,2};
	auto result = getDivisions(match,k);
	ASSERT_EQ(result.size(),8);

	ASSERT_TRUE((find(result.begin(), result.end(), set<char>()) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({0})) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({1})) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({2})) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({0,1})) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({0,2})) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({1,2})) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({0,1,2})) != result.end()));
}

TEST(GetDivisions, oneConnection) {
	int k = 5;
	char match[5] = {3,1,2,3,4};
	auto result = getDivisions(match,k);
	ASSERT_EQ(result.size(),2);

	ASSERT_TRUE((find(result.begin(), result.end(), set<char>()) != result.end()));
	ASSERT_TRUE((find(result.begin(), result.end(), set<char>({0})) != result.end()));
}

TEST(DivMatchSol, biggerSet) {
	int k = 8;
	char match[8] = {0,67,71,67,4,71,71,71};

	ASSERT_TRUE(ifDivisionMatch(set<char>(),match,k));
	ASSERT_TRUE(ifDivisionMatch(set<char>({1,4}),match,k));
	ASSERT_TRUE(ifDivisionMatch(set<char>({3,5,6,7}),match,k));

	ASSERT_FALSE(ifDivisionMatch(set<char>({0}),match,k));
	ASSERT_FALSE(ifDivisionMatch(set<char>({1,3}),match,k));
	ASSERT_FALSE(ifDivisionMatch(set<char>({4}),match,k));
	ASSERT_FALSE(ifDivisionMatch(set<char>({2,5,6,7}),match,k));
}

size_t getBucket(char const * solution, int k);
unordered_map<size_t,PartialSolutions> getBuckets(PartialSolutions solutions, int k);

//
// Gaussian elimination tests
//

TEST(Elimination, sameRows) {
	vector<vector<bool>> same_row_matrix;
	same_row_matrix.push_back(vector<bool>{true,false,false,true});
	same_row_matrix.push_back(vector<bool>{true,false,false,true});

	ASSERT_EQ(getSignificantRows(same_row_matrix).size(),(size_t)1);
}

//
// MAIN
//

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
