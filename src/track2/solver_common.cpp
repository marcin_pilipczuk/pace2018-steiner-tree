#include <algorithm>
#include <vector>
#include <memory>
#include <cmath>
#include <set>
#include <map>
#include <functional>
#include <unordered_map>
#include <cstdlib>
#include <cstring>
#include <cassert>

#include "basic.h"
#include "gaussian.h"
#include "solver_common.h"

#define FREQ 2

#define val(x) ((x)&0b11111)

using namespace std;

Edge::Edge() {
	this->edge = mp(-1,-1);
}

Edge::Edge(pair<int,int> const & edge, shared_ptr<Edge> pv) {
	this->edge = edge;
	this->previous = pv;
}

PartialSolution::PartialSolution(shared_ptr<Edge> e, size_t _cost) :
	edges(e),
	cost(_cost)
{
	for (int i = 0; i < PS_SIZE; i++)
		this->component[i] = 0;
}

PartialSolution::PartialSolution(shared_ptr<Edge> e) :
	edges(e)
{
	for (int i = 0; i < PS_SIZE; i++)
		this->component[i] = 0;
	this->cost = 0l;
}

PartialSolution::PartialSolution() {
	for (int i = 0; i < PS_SIZE; i++)
		this->component[i] = 0;
	this->cost = 0l;
}

//
// For sets
//

size_t hash<PartialSolution>::operator() (PartialSolution const & ps) const {
	size_t result = 0L;

	for (int i = 0; i < PS_SIZE; i++)
		result = (result * MUL + ps.component[i]) % MOD;

	return result;
}

bool operator==(PartialSolution const & A, PartialSolution const & B) {
	for (int i = 0; i < PS_SIZE; i++)
		if (A.component[i] != B.component[i])
			return false;
	return true;
}

bool operator!=(PartialSolution const & A, PartialSolution const & B) {
	return not (A==B);
}

bool operator<(PartialSolution const & A, PartialSolution const & B) {
	for (int i = 0; i < PS_SIZE; i++)
		if (A.component[i] < B.component[i]) {
			return true;
		} else if (A.component[i] > B.component[i]) {
			return false;
		} else {}

	return false;
}

//
// Add vertex
//

void addVertex(TWNode const & node, PartialSolutions & solutions) {
	int k = node.vertices.size();
	int v = node.v;

	char flag = 0;
	if (node.mode == TERMINAL)
		flag = 64;

	for (auto & solution : solutions) {
		for (int i = 0; i < k-1; i++)
			if (val(solution.component[i]) >= v)
				solution.component[i]++;

		for (int i = k; i > v; i--)
			solution.component[i] = solution.component[i-1];

		solution.component[v] = v + flag;
	}
}

//
// Add edge
// 

void addEdge(TWNode const & node, PartialSolutions & solutions) {
	auto e = mp(node.vertices[node.v],node.vertices[node.w]);
	int k = node.vertices.size();
	int v = node.v;
	int w = node.w;
	size_t cost = (size_t)node.cost;

	if (node.mode == DELETED)
		return;

	PartialSolutions new_solutions;
	for (auto & solution : solutions)
		if (solution.component[v] != solution.component[w]) {
			PartialSolution new_solution = solution;
			new_solution.cost += cost;
			new_solution.edges = shared_ptr<Edge>(new Edge(e,solution.edges));

			auto v_component = solution.component[v];
			auto w_component = solution.component[w];

			auto new_component = max(val(v_component),val(w_component)) + (v_component + w_component > 64 ? 64 : 0);

			for (int i = 0; i < k; i++)
				if (new_solution.component[i] == v_component or new_solution.component[i] == w_component)
					new_solution.component[i] = new_component;

			new_solutions.push_back(new_solution);
		}

	if (node.mode == AVAILABLE)
		solutions.insert(solutions.end(),new_solutions.begin(),new_solutions.end());
	else
		solutions.swap(new_solutions);
}

//
// Del vertex
//

PartialSolutions delVertex(TWNode const & node, PartialSolutions & solutions, size_t & result, shared_ptr<Edge> & result_edges) {
	int k = node.vertices.size();
	int v = node.v;

	PartialSolutions new_solutions;

	for (auto & solution : solutions) {
		auto v_component = solution.component[v];

		if (v_component > 31 and node.is_final) {
			int num = -1;
			for (int i = 0; i < k+1; i++)
				if (solution.component[i] > 31)
					num++;
			
			if (num == 0) {
				if (result > solution.cost) {
					result = solution.cost;
					result_edges = solution.edges;
				}
				continue;
			}
		}
		
		int num = -1;
		for (int i = 0; i < k+1; i++)
			if (v_component == solution.component[i])
				num++;

		if (num == 0 and v_component > 31)
			continue;

		if (num > 0) {
			int new_comp;
			int flag = 0;
			if (v_component > 31)
				flag = 64;
			
			for (int i = k; i >= 0; i--)
				if (solution.component[i] == v_component and i != v) {
					new_comp = flag+i;
					break;
				}
			
			for (int i = k; i >= 0; i--)
				if (solution.component[i] == v_component)
					solution.component[i] = new_comp;
		}

		for (int i = 0; i < k+1; i++)
			if (val(solution.component[i]) > v)
				solution.component[i]--;

		for (int i = v; i < k; i++)
			solution.component[i] = solution.component[i+1];
		solution.component[k] = 0;

		new_solutions.push_back(solution);
	}

	return new_solutions;
}

//
// Join
// 

bool checkEdges(shared_ptr<Edge> e) {
	set<pair<int,int>> S;

	while (e->edge != mp(-1,-1))
		if (S.count(e->edge) != 0)
			return false;
		else {
			S.insert(e->edge);
			e = e->previous;
		}

	return true;
}

void FindUnion::clear() {
	for (int i = 0; i < (int)par.size(); i++)
		par[i] = i;
}

FindUnion::FindUnion(int n) : par(vector<int>(n)) {}

int FindUnion::parent(int e) {
	if (par[e] == e)
		return e;
	par[e] = parent(par[e]);
	return par[e];
}

void FindUnion::join(int e1, int e2) {
	int p1 = parent(e1);
	int p2 = parent(e2);

	int p = max(val(p1),val(p2)) + (p1 >= 25 or p2 >= 25 ? 64 : 0);

	par[p1] = p;
	par[p2] = p;
	par[e1] = p;
	par[e2] = p;
}

shared_ptr<Edge> recreateSolution(Edge * base, shared_ptr<Edge> current, map<pair<Edge*,Edge*>,shared_ptr<Edge>> & created_edges) {
	auto key = mp(current.get(),base);
	if (created_edges.count(key) == 0)
		created_edges[key] = shared_ptr<Edge>(new Edge(current->edge,recreateSolution(base,current->previous,created_edges)));
	
	return created_edges[key];
}

PartialSolutions join(PartialSolution const & base_solution, PartialSolution solution, int k, FindUnion & fu_struct) {
	fu_struct.clear();

	for (int i = k-1; i >= 0; i--)
		if (fu_struct.parent(solution.component[i]) == fu_struct.parent(base_solution.component[i])
				and val(fu_struct.parent(solution.component[i])) != i)
			return PartialSolutions({});
		else
			fu_struct.join(solution.component[i],base_solution.component[i]);

	for (int i = 0; i < k; i++)
		solution.component[i] = fu_struct.parent(solution.component[i]);

	solution.cost = base_solution.cost + solution.cost;
	auto ptr = base_solution.edges.get();
	solution.edges = shared_ptr<Edge>(new Edge(*((std::pair<int,int>*)(&ptr)),solution.edges));

	return PartialSolutions({solution});
}

PartialSolutions join(TWNode const & node, PartialSolutions && solutions_a, PartialSolutions && solutions_b) {
	FindUnion fu_struct(100);
	map<pair<Edge*,Edge*>, shared_ptr<Edge>> created_edges;
	
	for (auto const & solution : solutions_a)
		created_edges[mp((Edge*)NULL,solution.edges.get())] = solution.edges;

	PartialSolutions result;
	for (auto const & solution_a : solutions_a)
		for (auto const & solution_b : solutions_b)
			for (auto & solution : join(solution_a,solution_b,node.vertices.size(),fu_struct)) {
				result.push_back(move(solution));
				if ((int)result.size() > (200<<node.vertices.size()))
					result = simpleReduction(move(result));
			}

	result = simpleReduction(move(result));
	for (auto & solution : result) 
		solution.edges = recreateSolution((Edge*)(*((void**)(&(solution.edges->edge)))),solution.edges->previous,created_edges);
	return result;
}

//
// Constructor
//

Solver::Solver(function<PartialSolutions(PartialSolutions,int)> r) :
	reduction(r)
{}

//
// Processong nodes
//

PartialSolutions Solver::process_node(int root, vector<TWNode> const & nodes) {

	int k = nodes[root].vertices.size();

	vector<PartialSolutions> partial_solutions;

	for (auto child : nodes[root].children)
		partial_solutions.push_back(process_node(child, nodes));

	PartialSolutions result;

	switch (nodes[root].type) {
		case ADD_VERTEX :
			result = move(partial_solutions[0]);
			addVertex(nodes[root],result);
			break;
		case ADD_EDGE :
			addEdge(nodes[root],partial_solutions[0]);
			result = simpleReduction(move(partial_solutions[0]));
			//result = reduction(move(partial_solutions[0]),k);
			break;
		case DEL_VERTEX :
			result = simpleReduction(delVertex(nodes[root],partial_solutions[0],this->result,this->result_edges));
			//result = delVertex(nodes[root],partial_solutions[0],this->result,this->result_edges);
			break;
		case JOIN :
			result = join(nodes[root],move(partial_solutions[0]),move(partial_solutions[1]));
			break;
		case LEAF :
			result = PartialSolutions({PartialSolution(NULL)});
			break;
		default :
			result = partial_solutions[0];
	}

	//printf("%d RES SIZE: %d\n", root, result.size());
	//return reduction(move(result),nodes[root].vertices.size());
	return result;
}

//
// Getting result
//

pair<size_t,EdgeSet> Solver::get(NiceDecomposition & decomposition) {
	this->result = 1000000000000000l;
	PartialSolutions solutions = process_node(0,decomposition);

	EdgeSet result;
	auto current = this->result_edges;
	while (current != NULL) {
		result.insert(current->edge);
		current = current->previous;
	}

	return mp(this->result,result);
}

//
// SIMPLE REDUCTION
//

PartialSolutions simpleReduction(PartialSolutions result) {
	if (result.size() <= 1)
		return result;

	sort(result.begin(), result.end());

	auto first_free = result.begin();
	for (auto ptr = result.begin(); ptr != prev(result.end()); ptr++)
		if (*ptr != *next(ptr)) {
			*first_free = *ptr;
			first_free++;
		} else if (ptr->cost < next(ptr)->cost) {
			*(next(ptr)) = *ptr;
		}
	*first_free = *prev(result.end());
	first_free++;

	if (first_free != prev(result.end()))
		result.erase(first_free, result.end());

	return result;
}

//
// REDUCTION
//

//
// Bucket for PartialSolution
//

size_t getBucket(char const * component, int k) {
	size_t result = 0l;

	for (int i = 0; i < k; i++)
		if (component[i] != i)
			result = result | (1 << i) | (1 << val(component[i]));

	return result;
}

unordered_map<size_t,PartialSolutions> getBuckets(PartialSolutions solutions, int k) {
	unordered_map<size_t,PartialSolutions> result;

	for (auto solution : solutions) {
		auto bucket = getBucket(solution.component, k);
		if (result.count(bucket) == 0)
			result[bucket] = PartialSolutions();
		result[bucket].push_back(solution);
	}

	return result;
}

vector<set<char>> getDivisions(set<char> s) {
	if (s.size() == 1)
		return vector<set<char>>({set<char>()});

	int v = *s.begin();

	s.erase(v);
	auto result = getDivisions(s);
	int init_size = result.size();

	for (int i = 0; i < init_size; i++) {
		auto new_one = result[i];
		new_one.insert(v);
		result.push_back(new_one);
	}

	return result;
}

vector<set<char>> getDivisions(const char * component, int k) {
	set<char> s;
	auto bucket = getBucket(component,k);

	for (int i = 0; i < k; i++) 
		if ((bucket & (1 << i)) > 0)
			s.insert(i);

	if (s.size() == 0)
		return vector<set<char>>({set<char>()});

	return getDivisions(s);
}

bool ifDivisionMatch(set<char> const & div, char const * component, int k) {
	if (div.size() == 0l or div.size() == (size_t)k)
		return true;

	for (int v = 0; v < k; v++) {
		if ((div.count(v) == 1 and div.count(val(component[v])) == 0)
				or (div.count(v) == 0 and div.count(val(component[v])) == 1))
			return true;
	}
	return false;
}

//
// Single edge vertices number
//

int sevNum(size_t group, int k) {
	int result = 0;
	while (group > 0l) {
		if (group % 2l == 1l)
			result++;
		group = group >> 1l;
	}
	return result;
}

//
// Reduction of weak partial solutions
//

bool isWeaker(PartialSolution const & A, PartialSolution const & B, int k) {
	if (A.cost < B.cost)
		return false;

	for (int i = 0; i < k; i++)
		if (B.component[i] != B.component[val(A.component[i])])
			return false;
	
	return true;
}

PartialSolutions reduceWeaker(PartialSolutions && solutions, int k) {
	sort(solutions.begin(), solutions.end(), [](auto const & A, auto const & B) {
		if (A.cost != B.cost)
			return A.cost < B.cost;
		else
			return A < B;
	});

	vector<bool> deleted(solutions.size(),false);

	for (int i = 0; i <= ((int)solutions.size())-2; i++)
		for (int j = i+1; j <= ((int)solutions.size())-1; j++) {
			if (isWeaker(solutions[i],solutions[j],k))
				deleted[i] = true;
			if (isWeaker(solutions[j],solutions[j],k))
				deleted[j] = true;
		}

	PartialSolutions result;
	for (int i = 0; i <= ((int)solutions.size())-1; i++)
		if (deleted[i])
			result.push_back(solutions[i]);

	return result;
}

//
// Reduction
//

PartialSolutions reduction(PartialSolutions solutions, int k) {
	solutions = simpleReduction(solutions);

	if (solutions.size() < (size_t)(1<<(k+2)))
		return solutions;

	auto buckets = getBuckets(solutions, k);

	PartialSolutions result;

	for (auto & p : buckets) {
		auto & bucket = p.second;

		if (bucket.size() == 0)
			continue;

		int sev_num = sevNum(p.first, k);
		//printf("SEV: %d", sev_num);
		if (sev_num <= 2 or bucket.size() < (size_t)(1<<(sev_num))) {
			result.insert(result.end(), bucket.begin(), bucket.end());
			continue;
		}

		auto divisions = getDivisions(bucket[0].component, k);

		sort(bucket.begin(),bucket.end(),[](auto A, auto B) {
			return A.cost < B.cost;			
		});

		vector<vector<bool>> matrix(bucket.size());
		for (int i = 0; i < ((int)bucket.size()); i++)
			for (auto div : divisions)
				matrix[i].push_back(ifDivisionMatch(div,bucket[i].component,k));

		auto nums = getSignificantRows(matrix);

		for (int num : nums)
			result.push_back(bucket[num]);
	}

	return result;
}
