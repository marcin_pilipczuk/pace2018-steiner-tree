#include "strange_io.h"
#include "treewidth.h"
#include "solver_common.h"

using namespace std;

int main() {
	[[maybe_unused]]
	auto [ _, edge_map, terminals, ordinary_decomposition ] = readInput(cin);

	auto decomposition = makeDecompositionNice(ordinary_decomposition,edge_map,terminals);

	auto solver = Solver(reduction);
	auto [ cost, edges ] = solver.get(decomposition);
	printOutput(cost,edges);
}
